#include "ReadPositionParser.h"
#include "../../framework/Plugin.h"
#include "../../framework/utils.h"
#include "../../framework/fragments/Fragment.cpp"

#include <boost/filesystem/operations.hpp>
#include <fstream>
#include <string>
#include <bitset>
#include <math.h>

using namespace std;

string ReadPositionParser::toNDigits(int value, int N, char fill_char) {
    stringstream ss;
    ss << setw(N) << setfill(fill_char) << value;
    return ss.str();
}

void ReadPositionParser::parsePosTxtFile(string pathToPosFile, vector<uint16_t> tiles, int lane,
                                         SerializableVector<float> *positionVector, bool isBgzfParser) {

    cout << "Parse *_pos.txt files for lane " << lane << endl;

    vector<string> fileNamePrefixes;
    transform(tiles.begin(), tiles.end(), back_inserter(fileNamePrefixes),
              [](uint16_t tile) -> string { return "_" + to_string(tile); });

    if (isBgzfParser){
        fileNamePrefixes = vector<string>(1, "");
    }

    for (auto const &fileNamePrefix : fileNamePrefixes) {
        string currentFile = pathToPosFile + "L" + toNDigits(lane, 3) + "/s_" + to_string(lane) + fileNamePrefix + "_pos.txt";

        ifstream posFile;

        if (!boost::filesystem::exists(currentFile)) {
            cerr << "Couldn't open " << currentFile << endl;
            return;
        }

        posFile.open(currentFile, ios::in);

        string line;

        // For an unique name
        int j = 0;

        while (getline(posFile, line)) {

            char delimiter = ' ';
            string xPos = line.substr(0, line.find(delimiter));

            std::string::size_type sx;
            float xFloat = stof(xPos, &sx);

            line.erase(0, line.find(delimiter) + 1);
            string yPos = line.substr(0, line.find(delimiter));

            std::string::size_type sy;
            float yFloat = stof(yPos, &sy);

            j++;

            positionVector->push_back(xFloat);
            positionVector->push_back(yFloat);
        }

        posFile.close();
    }
}

void ReadPositionParser::parseLocsFile(string pathToLocsFile, vector<uint16_t> tiles, int lane,
                                       SerializableVector<float> *positionVector, bool isBgzfParser) {

    cout << "Parse .locs files for lane " << lane << endl;

    int headerSize = 12;

    vector<string> fileNamePrefixes;
    transform(tiles.begin(), tiles.end(), back_inserter(fileNamePrefixes),
              [](uint16_t tile) -> string { return "_" + to_string(tile); });

    if (isBgzfParser){
        fileNamePrefixes = vector<string>(1, "");
    }

    for (auto const &fileNamePrefix : fileNamePrefixes) {

        string currentFile = pathToLocsFile + "L" + toNDigits(lane, 3) + "/s_" + to_string(lane) + fileNamePrefix + ".locs";

        if (!boost::filesystem::exists(currentFile)) {
            cerr << "Couldn't open " << currentFile << "!" << endl;
            return;
        }

        ifstream posFile(currentFile, ios::in | ios::binary);

        char headerBuffer[headerSize];

        posFile.read(headerBuffer, headerSize);

        // Parse the header information
        int controlNumber = ((headerBuffer[3] << 24) & 0xff000000) | ((headerBuffer[2] << 16) & 0x00ff0000) |
                            ((headerBuffer[1] << 8) & 0x0000ff00) | (headerBuffer[0] & 0x000000ff);

        // TODO: Is controlNumber needed?
        (void) controlNumber;

        // Union to calculate the version Number
        union versionNumber {
            char buf[4];
            float number;
        } versionNumber;

        // Should be equal 1.0
        versionNumber.buf[0] = headerBuffer[4];
        versionNumber.buf[1] = headerBuffer[5];
        versionNumber.buf[2] = headerBuffer[6];
        versionNumber.buf[3] = headerBuffer[7];

        // TODO: Is versionNumber needed?
        (void) versionNumber;

        // Convert the last 4 Bytes of the first 12 Bytes into an int (little endian)
        unsigned int clusterCount = ((headerBuffer[11] << 24) & 0xff000000) | ((headerBuffer[10] << 16) & 0x00ff0000) |
                                    ((headerBuffer[9] << 8) & 0x0000ff00) | (headerBuffer[8] & 0x000000ff);

        float pos;

        for (unsigned int j = 0; j < clusterCount * 2; j++) {
            posFile.read(reinterpret_cast<char *>(&pos), sizeof(float));
            positionVector->push_back(round(10 * pos + 1000));
        }
    }
}

void ReadPositionParser::parseClocsFile(string pathToClocsFile, vector<uint16_t> tiles, int lane,
                                        SerializableVector<float> *positionVector, bool isBgzfParser) {
    cout << "Parse .clocs files for lane " << lane << endl;

    int headerSize = 5;

    vector<string> fileNamePrefixes;
    transform(tiles.begin(), tiles.end(), back_inserter(fileNamePrefixes),
              [](uint16_t tile) -> string { return "_" + to_string(tile); });

    if (isBgzfParser){
        fileNamePrefixes = vector<string>(1, "");
    }

    for (auto const &fileNamePrefix : fileNamePrefixes) {
        string currentFile = pathToClocsFile + "L" + toNDigits(lane, 3) + "/s_" + to_string(lane) + fileNamePrefix + ".clocs";

        if (!boost::filesystem::exists(currentFile)) {
            cerr << "Couldn't open " << currentFile << "!" << endl;
            return;
        }

        ifstream posFile(currentFile, ios::in | ios::binary);

        char headerBuffer[headerSize];

        posFile.read(headerBuffer, headerSize);

        // MAX 255 since this is encoded in clocs files with only one Byte
        int binsCount = ((headerBuffer[4] << 24) & 0xff000000) | ((headerBuffer[3] << 16) & 0x00ff0000) |
                        ((headerBuffer[2] << 8) & 0x0000ff00) | (headerBuffer[1] & 0x000000ff);

        unsigned char byte;

        for (int j = 0; j < binsCount; j++) {

            posFile.read(reinterpret_cast<char *>(&byte), sizeof(char));

            int blockCount = byte;

            for (int k = 0; k < blockCount; k++) {
                posFile.read(reinterpret_cast<char *>(&byte), sizeof(char));
                auto xRelativeCoordinate = float(byte);

                posFile.read(reinterpret_cast<char *>(&byte), sizeof(char));
                auto yRelativeCoordinate = float(byte);

                float *realCoordinates = computeRealXYValue(xRelativeCoordinate, yRelativeCoordinate, binsCount, j);

                positionVector->push_back(realCoordinates[0]);
                positionVector->push_back(realCoordinates[1]);
            }
        }
    }
}

float *ReadPositionParser::computeRealXYValue(float xRelativeCoordinate, float yRelativeCoordinate, int binCount,
                                              int currentBin) {
    (void) binCount; // UNUSED

    static auto *pair = new float[2];
    static int xOffset = 0;
    static int yOffset = 0;
    int imageWidth = this->getConfigEntry<int>("imageWidth");
    int blockSize = this->getConfigEntry<int>("blockSize");

    auto maxBins = (int) (ceil((double) imageWidth / (double) blockSize));

    pair[0] = (xRelativeCoordinate / 10.0f + xOffset);
    pair[1] = (yRelativeCoordinate / 10.0f + yOffset);
    if (currentBin > 0 && ((currentBin + 1) % maxBins == 0)) {
        xOffset = 0;
        yOffset += blockSize;
    } else
        xOffset += blockSize;

    return pair;
}

void ReadPositionParser::setConfig() {
    this->registerConfigEntry<string>("positionPath", "NO_PATH");

    this->registerConfigEntry<int>("blockSize", 25);
    this->registerConfigEntry<int>("imageWidth", 2048);
}

FragmentContainer *ReadPositionParser::runPreprocessing(FragmentContainer *inputFragments) {
    (void) inputFragments; // UNUSED

    cout
            << endl
            << "------------------------------------------------------------------" << endl
            << "Read Position Parser" << endl
            << "------------------------------------------------------------------" << endl;

    auto outputContainer = this->framework->createNewFragmentContainer();
    outputContainer->add(this->framework->createNewFragment("POSITION"));

    // Define important variables
    vector<uint16_t> tiles = this->getConfigEntry<vector<uint16_t>>("tiles");
    shared_ptr<Fragment> readPosition = outputContainer->get("POSITION");

    auto *positionVector = new SerializableVector<float>();
    readPosition->setSerializable("positionVector", positionVector);

    // Choose the available position file format
    ifstream file;
    string positionPath = this->getConfigEntry<string>("positionPath");
    string filePath;

    bool isBgzfParser = this->getConfigEntry<string>("BclParserType") == "BgzfBclParser";
    string fileNamePrefix = isBgzfParser ? "" : "_1101";

    for (auto lane : this->getConfigEntry<vector<uint16_t >>("lanes")) {

        // .*_pos.txt
        filePath = positionPath + "L" + toNDigits(lane, 3) + "/s_" + to_string(lane) + fileNamePrefix + "_pos.txt";
        if (boost::filesystem::exists(filePath)) {
            this->parsePosTxtFile(positionPath, tiles, lane, positionVector, isBgzfParser);
            continue;
        }

        // .locs
        filePath = positionPath + "L" + toNDigits(lane, 3) + "/s_" + to_string(lane) + fileNamePrefix + ".locs";
        if (boost::filesystem::exists(filePath)) {
            this->parseLocsFile(positionPath, tiles, lane, positionVector, isBgzfParser);
            continue;
        }

        // .clocs
        filePath = positionPath + "L" + toNDigits(lane, 3) + "/s_" + to_string(lane) + fileNamePrefix + ".clocs";
        if (boost::filesystem::exists(filePath)) {
            this->parseClocsFile(positionPath, tiles, lane, positionVector, isBgzfParser);
            continue;
        }

        // position files don't exists
        cerr << "Couldn't find position files (locs, clocs, *_pos.txt) for lane " + to_string(lane) + ": ";
    }

    cout << endl;
    return outputContainer;
}

extern "C" ReadPositionParser *create() {
    return new ReadPositionParser;
}

extern "C" void destroy(ReadPositionParser *plugin) {
    delete plugin;
}
