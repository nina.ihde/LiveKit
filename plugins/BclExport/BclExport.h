#ifndef LIVEKIT_BCLEXPORT_H
#define LIVEKIT_BCLEXPORT_H

#include <iostream>
#include "../../framework/Plugin.h"
#include "../../framework/FrameworkInterface.h"
#include "../../framework/fragments/Fragment.cpp"

class BclExport final : public Plugin {
public:
    FragmentContainer *runPreprocessing(FragmentContainer *inputFragments) override;

    FragmentContainer *runCycle(FragmentContainer *inputFragments) override;

    FragmentContainer *runFullReadPostprocessing(FragmentContainer *inputFragments) override;

    void setConfig() override;

};

#endif //LIVEKIT_BCLEXPORT_H
