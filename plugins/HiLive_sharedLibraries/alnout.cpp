#include "alnout.h"

//-------------------------------------------------------------------//
//------  Streamed SAM generation -----------------------------------//
//-------------------------------------------------------------------//
using namespace std;

AlnOut::AlnOut(
        std::vector<CountType> lns,
        std::vector<CountType> tls,
        CountType cycle,
        Configurable *settings,
        SerializableSequenceElements *sequenceElements,
        BaseManager *baseManager,
        SerializableIndex *index,
        int mateCount
) : cycle(cycle), settings(settings), sequenceElements(sequenceElements), baseManager(baseManager), index(index),
    outputMode(settings->getConfigEntry<OutputMode>("mode")) {

    // Fill list of specified barcodes
    std::vector<std::string> barcodes;
    for (unsigned int i = 0; i < settings->getConfigEntry<vector<vector<string>>>("barcodeVector").size(); i++) {
        barcodes.push_back(get_barcode_string(i));
    }

    // Get the finished cycles for each mate
    for (CountType mate = 1; mate <= mateCount; mate++) {
        mateCycles.push_back(this->sequenceElements->getMateCycle(mate, cycle));
    }

    // Add a waiting task for all lanes and tiles.
    for (auto ln : lns) {
        for (auto tl : tls) {
            add_task(Task(ln, tl, cycle), WAITING);
        }
    }

};

AlnOut::AlnOut() {
    this->finalized = true;
}

AlnOut::~AlnOut() {
    if (!is_finalized() && !finalize()) {
        std::cerr << "Could not finish output for cycle " << cycle << "." << std::endl;
    }
}

void AlnOut::init() {

    std::lock_guard<std::mutex> lock(if_lock);

    if (initialized)
        return;

    barcodes = get_barcode_string_vector();

    // Init the bamIOContext (the same object can be used for all output streams)
    bfos.set_context(this->index->getSeqNames(), this->index->getSeqLengths());

    // Init the header (the same object can be used for all output streams)
    seqan::BamHeader header = getBamHeader();

    // Init output stream for each barcode (plus undetermined if keep_all_barcodes is set)
    for (unsigned int barcode = 0; barcode < (barcodes.size() + 1); barcode++) {
        if (barcode < barcodes.size() || settings->getConfigEntry<bool>("keepAllBarcodes")) {

            std::string barcode_string = (barcode == barcodes.size()) ? "undetermined" : barcodes[barcode];

            // Open file in Bam output stream and write the header
            bfos.emplace_back(getBamTempFileName(barcode_string, cycle).c_str());
            bfos[barcode].writeHeader(header);

        }
    }

    initialized = true;
}

bool AlnOut::set_task_status(Task t, ItemStatus status) {
    std::lock_guard<std::mutex> lock(tasks_lock);
    if (tasks.find(t) == tasks.end())
        return false;
    tasks[t] = status;
    return true;
}

bool AlnOut::set_task_status_from_to(Task t, ItemStatus oldStatus, ItemStatus newStatus) {
    std::lock_guard<std::mutex> lock(tasks_lock);
    if (tasks.find(t) == tasks.end())
        return false;
    if (tasks[t] == oldStatus) {
        tasks[t] = newStatus;
        return true;
    }
    return false;
}

Task AlnOut::get_next(ItemStatus getStatus, ItemStatus setToStatus) {
    std::lock_guard<std::mutex> lock(tasks_lock);
    for (auto it = tasks.begin(); it != tasks.end(); ++it) {
        if (it->second == getStatus) {
            tasks[it->first] = setToStatus;
            return it->first;
        }
    }
    return NO_TASK;
}

bool AlnOut::add_task(Task t, ItemStatus status) {
    std::lock_guard<std::mutex> lock(tasks_lock);
    if (tasks.find(t) != tasks.end())
        return false;
    tasks[t] = status;
    return true;
}

bool AlnOut::sort_tile(CountType ln, CountType tl, CountType mate, CountType cycle, bool overwrite) {

    string tempDir = settings->getConfigEntry<string>("tempDir");
    std::string in_fname = get_align_fname(ln, tl, cycle, mate, tempDir);
    std::string out_fname = get_align_fname(ln, tl, cycle, mate, tempDir) + ".sorted";

    // Stop if sorted file already exist
    if (file_exists(out_fname) && !overwrite)
        return true;

    ReadAlignmentSettings readAlignmentSettings = ReadAlignment::generateSettingsfromConfigurable(settings);

    iAlnStream input(settings->getConfigEntry<uint64_t>("blockSize"),
                     settings->getConfigEntry<uint8_t>("compressedFormat"), &readAlignmentSettings);

    input.open(in_fname);

    assert(input.get_cycle() == cycle);
    assert(input.get_lane() == ln);
    assert(input.get_tile() == tl);

    uint32_t num_reads = input.get_num_reads();

    oAlnStream output(ln, tl, cycle, input.get_rlen(), num_reads, settings->getConfigEntry<uint64_t>("blockSize"),
                      settings->getConfigEntry<uint8_t>("compressedFormat"));
    output.open(out_fname);

    for (uint32_t i = 0; i < num_reads; i++) {

        try {
            ReadAlignment *ra = input.get_alignment();
            ra->sort_seeds_by_as();
            output.write_alignment(ra);
            delete ra;
        } catch (const std::exception &ex) {
            return false;
        }

    }

    if (!(input.close() && output.close()))
        return false;

    return true;
}

CountType
AlnOut::openiAlnStream(CountType lane, CountType tile, CountType mateCycle, CountType mate, iAlnStream *istream) {

    // Check if mate is valid.
    if (this->sequenceElements->getSeqByMate(mate) == NULLSEQ)
        return 1;

    // Check if mate cycle is >0
    if (mateCycle == 0)
        return 2;

    // Sort file if necessary
    if (!sort_tile(lane, tile, mate, mateCycle, settings->getConfigEntry<bool>("forceResort")))
        return 3;

    // Open sorted alignment file
    string tempDir = settings->getConfigEntry<string>("tempDir");
    std::string alignment_fname = get_align_fname(lane, tile, mateCycle, mate, tempDir) + ".sorted";

    // Check if sorted file exist
    if (!file_exists(alignment_fname)) {
        return 3;
    }

    // Open the stream
    istream->open(alignment_fname);

    return 0;
}

std::vector<iAlnStream *> AlnOut::openiAlnStreams(CountType lane, CountType tile, unsigned filter_size) {

    std::vector<iAlnStream *> alignmentFiles;
    unsigned numberOfAlignments = 0;

    this->readAlignmentSettings = ReadAlignment::generateSettingsfromConfigurable(settings);

    for (unsigned int mateIndex = 1; mateIndex <= mateCycles.size(); mateIndex++) {

        // Init the stream object
        iAlnStream *input = new iAlnStream(settings->getConfigEntry<uint64_t>("blockSize"),
                                           settings->getConfigEntry<uint8_t>("compressedFormat"),
                                           &readAlignmentSettings);

        // Try to open the stream
        CountType ret = openiAlnStream(lane, tile, mateCycles[mateIndex - 1], mateIndex, input);

        // On success
        if (ret == 0) {

            // compare number of reads in alignment file with number of reads in filter file, if filter file exists
            if (input->get_num_reads() != filter_size) {
                std::stringstream msg;
                msg << "Unequal number of reads in filter file (" << filter_size << ") and alignment file ("
                    << input->get_num_reads() << ")";
                throw std::length_error(msg.str());
            }

            // compare number of reads in alignment file with number of reads in previous alignment file
            if (mateIndex != 1 && input->get_num_reads() != numberOfAlignments) {
                throw std::length_error("Unequal number of reads (between mates)");
            }

            alignmentFiles.push_back(input);
            numberOfAlignments = input->get_num_reads();
            continue;
        }

            // Throw an exception if a mate is not valid (should not happen)
        else if (ret == 1) {
            throw std::runtime_error("Mate number is not valid: " + std::to_string(mateIndex));
        }

            // Mate not sequenced yet (mateCycle==0)
        else if (ret == 2) {
            continue;
        }

            // Sorted file is not available
        else if (ret == 3) {
            throw std::runtime_error("Sorted align file not found.");
        }

            // Undefined return value
        else {
            throw std::logic_error("Undefined return value of function AlnOut::openiAlnStream: " + std::to_string(ret));
        }

    }

    return alignmentFiles;

}

void AlnOut::write_tile_to_bam(Task t) {

    if (!is_initialized()) init();

    try {
        __write_tile_to_bam__(t);
        set_task_status(t, FINISHED);
    } catch (const std::exception &e) {
        set_task_status(t, FAILED);
        std::cerr << "Writing of task " << t << " failed: " << e.what() << std::endl;
    }
}

void AlnOut::__write_tile_to_bam__(Task t) {

    //TODO: Write real paired-end output (?)

    CountType lane = t.lane;
    CountType tile = t.tile;

    ////////////////////////////////////////////////////
    //  Main loop //////////////////////////////////////
    ////////////////////////////////////////////////////

    // Open the input streams for the sorted alignment files.
    std::vector<iAlnStream *> alignmentFiles = openiAlnStreams(lane, tile,
                                                               this->baseManager->getNumSequences(lane, tile));
    unsigned numberOfAlignments = alignmentFiles[0]->get_num_reads();

    bool keepAllBarcodes = this->settings->getConfigEntry<bool>("keepAllBarcodes"),
            reportUnmapped = settings->getConfigEntry<bool>("reportUnmapped"),
            keepAllSequences = settings->getConfigEntry<bool>("keepAllSequences");

    ScoreType minAs = settings->getConfigEntry<ScoreType>("minAs");
    float maxSoftclipRatio = settings->getConfigEntry<float>("maxSoftclipRatio");
    uint16_t bestN = settings->getConfigEntry<uint16_t>("bestN");

    // for all reads in a tile
    /////////////////////////////////////////////////////////////////////////////
    for (uint64_t i = 0; i < numberOfAlignments; i++) {

        int mateIndex = 1;
        std::vector<ReadAlignment *> mateAlignments;
        for (auto e: alignmentFiles) {
            mateAlignments.push_back(e->get_alignment());
            int currentReadId = this->sequenceElements->getSeqByMate(mateIndex++).readId;
            mateAlignments.back()->setRead(this->baseManager->getSequence(lane, tile, i).getAddressAt(currentReadId));
        }

        std::vector<std::vector<seqan::BamAlignmentRecord>> mateRecords(mateAlignments.size(),
                                                                        std::vector<seqan::BamAlignmentRecord>(0));

        // if the filter file is available and the filter flag is 0 then skip
        if (this->baseManager->filterBasecall(lane, tile, i))
            continue;

        // compute barcode sequence as it should be written to BC tag
        std::string barcode = this->format_barcode(mateAlignments[0]->getBarcodeString());

        // Barcode index for the read
        CountType barcodeIndex = mateAlignments[0]->getBarcodeIndex();

        // If read has undetermined barcode and keep_all_barcodes is not set, skip this read
        if (barcodeIndex == UNDETERMINED && !keepAllBarcodes)
            continue;
        else if (barcodeIndex == UNDETERMINED)
            barcodeIndex = barcodes.size(); // this is the index for the "undetermined" output stream

        // setup QNAME
        // Read name format <instrument‐name>:<run ID>:<flowcell ID>:<lane‐number>:<tile‐number>:<x‐pos>:<y‐pos>
        // readname << "<instrument>:<run-ID>:<flowcell-ID>:" << ln << ":" << tl << ":<xpos>:<ypos>:" << i;
        //TODO: Get coordinates from clocs file, run-ID from runInfo.xml, flowcell ID from runInfo.xml, instrument from runInfo.xml
        std::stringstream readname;
        readname << "lane." << lane << "|tile." << tile << "|read." << i;

        // Track equivalent alignments for the same mate (similar positions from different seeds -> only keep the best one)
        // TODO: implement equivalent alignment window as user parameter
        PositionType equivalentAlignmentWindow = 10;

        // TODO: Improve the prevention of reporting similar alignments.
        // std::set<PositionType> alignmentPositions;
        std::set<GenomePosType> alignmentPositions;

        // for all mates
        /////////////////////////////////////////////////////////////////////////////
        for (unsigned int mateAlignmentIndex = 0; mateAlignmentIndex < mateAlignments.size(); ++mateAlignmentIndex) {

            // Decrease the ReadAlignment cycle since it is automatically increased when loading the file TODO: this should be changed.
            mateAlignments[mateAlignmentIndex]->cycle -= 1;

            // Init record with information about the read
            seqan::BamAlignmentRecord mate_record;
            mate_record.qName = readname.str();
            mate_record.flag = 0;

            // Set correct segment (paired) flags for the record
            if (mateAlignments.size() > 1) { // if there are at least two mates already sequenced

                mate_record.flag = addSAMFlag(mate_record.flag, SAMFlag::MULT_SEG);
                if (mateAlignmentIndex == 0) {
                    mate_record.flag |= addSAMFlag(mate_record.flag, SAMFlag::FIRST_SEG);
                } else if (mateAlignmentIndex == mateAlignments.size() - 1) {
                    mate_record.flag = addSAMFlag(mate_record.flag, SAMFlag::LAST_SEG);
                } else {
                    mate_record.flag = addSAMFlag(mate_record.flag, SAMFlag::FIRST_SEG);
                    mate_record.flag = addSAMFlag(mate_record.flag, SAMFlag::LAST_SEG);
                }

            }

            // Add read specific information to the BAM record
            mateAlignments[mateAlignmentIndex]->addReadInfoToRecord(mate_record);

            // Alignment disabled or no seeds
            if (mateAlignments[mateAlignmentIndex]->is_disabled() ||
                mateAlignments[mateAlignmentIndex]->seeds.size() == 0) {

                // Don't report disabled reads if their sequences are not kept.
                if (mateAlignments[mateAlignmentIndex]->is_disabled() && !keepAllSequences)
                    continue;

                // Report unmapped reads if activated
                if (reportUnmapped) {
                    mate_record.flag = addSAMFlag(mate_record.flag, SAMFlag::SEG_UNMAPPED);
                    mateRecords[mateAlignmentIndex].push_back(mate_record);
                }
                continue;
            }

            // Variables for output modes
            ScoreType first_seed_score = 0;
            ScoreType last_seed_score = 0;
            CountType num_diff_scores = 0;

            // Number of printed alignments for the current mate.
            unsigned printedMateAlignments = 0;

            // Unique mode interruption
            // TODO: Think about reporting of unmapped and non-unique reads if report-unmapped is activated
            // TODO: this filtering approach is problematic if similar seeds exist ...
            if (this->is_mode(UNIQUE) && (
                    mateAlignments[mateAlignmentIndex]->seeds.size() > 1 ||
                    (mateAlignments[mateAlignmentIndex]->seeds.size() == 1 &&
                     mateAlignments[mateAlignmentIndex]->seeds.front()->getNumPositions() > 1))) {
                continue;
            }

            std::vector<uint8_t> mapqs = mateAlignments[mateAlignmentIndex]->getMAPQs();
            auto mapqs_it = mapqs.begin();

            // for all seeds
            /////////////////////////////////////////////////////////////////////////////
            for (SeedVecIt it = mateAlignments[mateAlignmentIndex]->seeds.begin();
                 it != mateAlignments[mateAlignmentIndex]->seeds.end(); ++it, ++mapqs_it) {

                ScoreType curr_seed_score = (*it)->get_as();

                // If no alignment was printed before, the current one has the best "score"
                if (printedMateAlignments == 0)
                    first_seed_score = curr_seed_score;

                // Stop in all best mode when AS:i score is lower than the first
                if (this->is_mode(ALLBEST) && first_seed_score > curr_seed_score) {
                    goto nextmate;
                }

                // Don't write this seed if the user-specified score or softclip ratio is not fulfilled
                CountType softclip_length = (*it)->get_softclip_length();
                if (curr_seed_score < minAs || softclip_length > maxSoftclipRatio * mateCycles[mateAlignmentIndex]) {
                    continue;
                }

                // get CIGAR-String
                seqan::String<seqan::CigarElement<> > cigar = (*it)->returnSeqanCigarString();

                // Get NM:i value
                unsigned nm = (*it)->get_nm();


                // check if cigar string sums up to read length
                // TODO Potentially conflicts with the 'eachMateAligned' flag if done here.
                unsigned cigarElemSum = 0;
                unsigned deletionSum = 0;
                unsigned supposed_cigar_length = mateCycles[mateAlignmentIndex];

                for (seqan::Iterator<seqan::String<seqan::CigarElement<> > >::Type elem = seqan::begin(cigar);
                     elem != end(cigar); ++elem) {
                    if ((elem->operation == 'M') || (elem->operation == 'I') || (elem->operation == 'S') ||
                        (elem->operation == '=') || (elem->operation == 'X'))
                        cigarElemSum += elem->count;

                    if (elem->operation == 'D') {
                        deletionSum += elem->count;
                    }
                }
                if (cigarElemSum != supposed_cigar_length) {
                    it = mateAlignments[mateAlignmentIndex]->seeds.erase(it);
                    it--;
                    continue;
                }
                if (deletionSum >= supposed_cigar_length) {
                    it = mateAlignments[mateAlignmentIndex]->seeds.erase(it);
                    it--;
                    continue;
                }

                // Get positions for the current seed
                std::vector<GenomePosType> pos_list;

                if (this->is_mode(ANYBEST)) {
                    pos_list = (*it)->getPositions(0, 1, this->index); // retrieve only one position from the index
                } else
                    pos_list = (*it)->getPositions(this->index); // retrieve all positions from the index

                // handle all positions
                for (auto p = pos_list.begin(); p != pos_list.end(); ++p) {

                    // Stop in any best mode when first alignment was already written
                    if (this->is_mode(ANYBEST) && printedMateAlignments >= 1) {
                        goto nextmate;
                    }

                    // Stop in bestn mode if first n alignments were already written
                    if (this->is_mode(BESTN) && printedMateAlignments >= bestN) {
                        goto nextmate;
                    }

                    seqan::BamAlignmentRecord record = mate_record;

                    record.rID = CountType(p->gid / 2);

                    record.beginPos = mateAlignments[mateAlignmentIndex]->get_SAM_start_pos(*p, *it, this->index);

                    record.mapQ = *mapqs_it;

                    // skip invalid positions
                    if (record.beginPos < 0 ||
                        PositionType(record.beginPos) == std::numeric_limits<PositionType>::max()) {
                        continue;
                    }

                    // skip positions that were already written (equivalent alignments). This can be done because the best alignment for this position is written first.
                    if (alignmentPositions.find(
                            GenomePosType(p->gid, record.beginPos - (record.beginPos % equivalentAlignmentWindow))) !=
                        alignmentPositions.end() ||
                        alignmentPositions.find(GenomePosType(p->gid, record.beginPos + (equivalentAlignmentWindow -
                                                                                         (record.beginPos %
                                                                                          equivalentAlignmentWindow)))) !=
                        alignmentPositions.end()) {
                        continue;
                    }

                    record.cigar = cigar;
                    if (this->index->isReverse(p->gid))
                        seqan::reverse(record.cigar);

                    if (printedMateAlignments > 0) { // if current seed is secondary alignment
                        record.flag = addSAMFlag(record.flag, SAMFlag::SEC_ALIGNMENT);
                        seqan::clear(record.seq);
                        seqan::clear(record.qual);
                    }

                    if (this->index->isReverse(p->gid)) { // if read matched reverse complementary
                        seqan::reverseComplement(record.seq);
                        seqan::reverse(record.qual);
                        record.flag = addSAMFlag(record.flag, SAMFlag::SEQ_RC);
                    }

                    // Dictionary for additional SAM tags
                    seqan::BamTagsDict dict;

                    // Alignment Score
                    seqan::appendTagValue(dict, "AS", curr_seed_score);

                    // Barcode sequence
                    if (barcode != "")
                        seqan::appendTagValue(dict, "BC", barcode);

                    // Number of mismatches
                    seqan::appendTagValue(dict, "NM", nm);

                    // MD:Z string
                    std::string mdz = (*it)->getMDZString();
                    if (this->index->isReverse(p->gid))
                        mdz = reverse_mdz(mdz);
                    seqan::appendTagValue(dict, "MD", mdz);

                    record.tags = seqan::host(dict);

                    // fill records list
                    mateRecords[mateAlignmentIndex].push_back(record);

                    // set variables for mode selection
                    if (last_seed_score != curr_seed_score || num_diff_scores == 0)
                        ++num_diff_scores;
                    last_seed_score = curr_seed_score;

                    ++printedMateAlignments;
                    alignmentPositions.insert(
                            GenomePosType(p->gid, record.beginPos - (record.beginPos % equivalentAlignmentWindow)));

                }
            }
            nextmate:
            {
                // Report unmapped reads if activated
                if (mateRecords[mateAlignmentIndex].size() == 0 && reportUnmapped) {
                    mate_record.flag = addSAMFlag(mate_record.flag, SAMFlag::SEG_UNMAPPED);
                    mateRecords[mateAlignmentIndex].push_back(mate_record);
                }
            };
        }

        // Set flags related to the next mate.
        setMateSAMFlags(mateRecords);

        // Write all records as a group to keep suboptimal alignments and paired reads together.
        bfos[barcodeIndex].writeRecords(mateRecords);

        for (auto e:mateAlignments)
            delete e;

    }
    for (auto e:alignmentFiles)
        delete e;

    return;
}

void AlnOut::setMateSAMFlags(std::vector<std::vector<seqan::BamAlignmentRecord>> &mateRecords) {
    for (CountType i = 0; i < mateRecords.size(); i++) {

        // Stop if not paired
        if (mateRecords.size() <= 1)
            break;

        // Set related mate index
        CountType next = i == mateRecords.size() - 1 ? 0 : i + 1;

        for (auto &record_pointer : mateRecords[i]) {

            if (mateRecords[next].size() > 0) {

                CountType mateFlag = mateRecords[next][0].flag;

                // set next mate rID
                record_pointer.rNextId = mateRecords[next][0].rID;

                // set next mate pos
                record_pointer.pNext = mateRecords[next][0].beginPos;

                // other mate entry is reverse complemented
                if (hasSAMFlag(mateFlag, SAMFlag::SEQ_RC)) {
                    record_pointer.flag = addSAMFlag(record_pointer.flag, SAMFlag::NEXT_SEQ_RC);
                }

                // other mate entry is flagged as unmapped
                if (hasSAMFlag(mateFlag, SAMFlag::SEG_UNMAPPED)) {
                    record_pointer.flag = addSAMFlag(record_pointer.flag, SAMFlag::NEXT_SEG_UNMAPPED);

                    record_pointer.pNext = record_pointer.beginPos;
                    record_pointer.rNextId = record_pointer.rID;

                    // the current read is flagged as unmapped but the mate is not
                } else if (hasSAMFlag(record_pointer.flag, SAMFlag::SEG_UNMAPPED)) {
                    record_pointer.beginPos = mateRecords[next][0].beginPos;
                    record_pointer.rID = mateRecords[next][0].rID;
                }


                // No record for the mate available (this implies, that it is unmapped)
            } else {
                record_pointer.flag = addSAMFlag(record_pointer.flag, SAMFlag::NEXT_SEG_UNMAPPED);
            }
        }

    }
}

Task AlnOut::write_next() {
    if (this->is_finished()) return NO_TASK;

    Task t = get_next(BCL_AVAILABLE, RUNNING);
    if (t != NO_TASK) {
        write_tile_to_bam(t);
        if (this->is_finished()) this->finalize();
        return t;
    } else {
        return NO_TASK;
    }
}

string AlnOut::getBamFileName(std::string barcode, CountType cycle) {
    std::ostringstream fname;
    std::string file_suffix = getOutputFormat() == OutputFormat::BAM ? ".bam" : ".sam";
    fname << settings->getConfigEntry<string>("outDir") << "/hilive_out_" << "cycle" << std::to_string(cycle) << "_"
          << barcode << file_suffix;
    return fname.str();
}

string AlnOut::getBamTempFileName(std::string barcode, CountType cycle) {
    std::ostringstream fname;
    std::string file_suffix = getOutputFormat() == OutputFormat::BAM ? ".bam" : ".sam";
    fname << settings->getConfigEntry<string>("outDir") << "/hilive_out_" << "cycle" << std::to_string(cycle) << "_"
          << barcode << ".temp" << file_suffix;
    return fname.str();
}

OutputFormat AlnOut::getOutputFormat() {
    return settings->getConfigEntry<OutputFormat>("outputFormat");
}

std::vector<std::string> AlnOut::get_barcode_string_vector() {
    std::vector<std::string> bc_strings;
    for (CountType i = 0; i < settings->getConfigEntry<vector<vector<string>>>("barcodeVector").size(); i++) {
        bc_strings.push_back(get_barcode_string(i));
    }
    return bc_strings;
}

std::string AlnOut::get_barcode_string(CountType index) const {

    // invalid index
    if (index >= settings->getConfigEntry<vector<vector<string>>>("barcodeVector").size()) {
        return "";
    } else {

        std::vector<std::string> bc_vec = settings->getConfigEntry<vector<vector<string>>>("barcodeVector")[index];

        std::stringstream ss;
        for (auto fragment : bc_vec) {
            ss << fragment;
        }
        std::string barcode_string = ss.str();
        return this->format_barcode(barcode_string);
    }
}

std::string AlnOut::format_barcode(std::string unformatted_barcode) const {
    CountType pos = 0;
    for (auto el : this->sequenceElements->getSeqs()) {
        if (el.mate == 0) {
            pos += el.length;
            if (unformatted_barcode.length() >= pos)
                unformatted_barcode.insert(pos++, "-");
            else
                break;
        }
    }

    return unformatted_barcode.substr(0, pos - 1);
}

CountType AlnOut::get_task_status_num(ItemStatus getStatus) {
    CountType num = 0;
    std::lock_guard<std::mutex> lock(tasks_lock);
    for (auto it = tasks.begin(); it != tasks.end(); ++it) {
        if (it->second == getStatus) {
            num += 1;
        }
    }
    return num;
}

bool AlnOut::is_mode(OutputMode alignment_mode) const {
    return this->outputMode == alignment_mode;
}

bool AlnOut::finalize() {

    std::lock_guard<std::mutex> lock(if_lock);

    if (finalized)
        return true;

    // Don't finish if there are unfinished tasks.
    if (!is_finished())
        return false;

    bool success = true;

    bfos.clear();

    // Move all output files to their final location.
    for (unsigned int barcode = 0; barcode < barcodes.size() + 1; barcode++) {
        if (barcode < barcodes.size() || settings->getConfigEntry<bool>("keepAllBarcodes")) {

            std::string barcode_string = (barcode == barcodes.size()) ? "undetermined" : barcodes[barcode];

            int rename = atomic_rename(getBamTempFileName(barcode_string, cycle).c_str(),
                                       getBamFileName(barcode_string, cycle).c_str());
            if (rename == -1) {
                std::cerr << "Renaming temporary output file " << getBamTempFileName(barcode_string, cycle).c_str()
                          << " to " << getBamFileName(barcode_string, cycle).c_str() << " failed." << std::endl;
                success = false;
            }
        }
    }

    // If it comes here, it counts as finalized independently from the success state (otherwise, contradictory error messages may occur).
    finalized = true;

    return success;
}
