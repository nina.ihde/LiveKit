#include "alnread.h"

using namespace std;

seqan::String<seqan::CigarElement<> > Seed::returnSeqanCigarString() const {

    bool extended_cigar = this->settings->extendedCigar;

    typedef seqan::String<seqan::CigarElement<> > TSeqanCigarString;
    TSeqanCigarString seqanCigarString;
    seqan::CigarElement<> cigarElem;

    for (CigarVector::const_iterator it = cigar_data.begin(); it != cigar_data.end(); ++it) {

        // Alignment begins with no match => Softclip
        if ((*it).operation == SOFTCLIP) {
            cigarElem.operation = 'S';
            cigarElem.count = (*it).length;
            seqan::appendValue(seqanCigarString, cigarElem);
            continue;
        }

        // Mismatch => Alignment match
        if ((*it).operation == NO_MATCH) {
            cigarElem.operation = extended_cigar ? 'X' : 'M';
            cigarElem.count = (*it).length;
            seqan::appendValue(seqanCigarString, cigarElem);
            continue;
        }

        // Deletion
        else if ((*it).operation == DELETION) {
            cigarElem.operation = 'D';
            cigarElem.count = (*it).length;
            seqan::appendValue(seqanCigarString, cigarElem);
            continue;
        }

        // Insertion
        else if ((*it).operation == INSERTION) {
            cigarElem.operation = 'I';
            cigarElem.count = (*it).length;
            seqan::appendValue(seqanCigarString, cigarElem);
            continue;
        }

        // Match
        else {
            cigarElem.operation = extended_cigar ? '=' : 'M';
            cigarElem.count = (*it).length;
            seqan::appendValue(seqanCigarString, cigarElem);
            continue;

        }
    }

    // collapse Neighboring match regions
    for (unsigned k = 1; k < length(seqanCigarString); k++)
        if ((seqanCigarString[k - 1].operation == 'M') && (seqanCigarString[k].operation == 'M')) {
            unsigned temp = seqanCigarString[k].count;
            erase(seqanCigarString, k);
            seqanCigarString[k - 1].count += temp;
            k--;
        }

    return seqanCigarString;
}

void Seed::cout() const {
    std::cout << "----- SEED START -----" << std::endl;
    std::cout << "Max. alignment score: " << this->max_as << std::endl;
    std::cout << "Actual alignment score: " << this->get_as() << std::endl;
    std::cout << "CIGAR: ";
    for (auto el : this->cigar_data) {
        std::cout << el.length;
        if (el.operation == NO_MATCH) {
            std::cout << "X ";
        } else if (el.operation == SOFTCLIP) {
            std::cout << "S ";
        } else if (el.operation == INSERTION) {
            std::cout << "I ";
        } else if (el.operation == DELETION) {
            std::cout << "D ";
        } else {
            std::cout << "M(" << el.operation << ") ";
        }
    }
    std::cout << std::endl << "------ SEED END ------" << std::endl;
}

uint16_t Seed::serialize_size() const {

    // Calculate total size
    uint16_t total_size = 0;

    // Size of both vertex descriptors of the bidirectional index
    total_size += 2 * sizeof(FMVertexDescriptor);

    // Maximal AS
    total_size += sizeof(ScoreType);

    if (cigar_data.size() >= 256)
        throw std::overflow_error("CIGAR information contains more than 255 elements!");

    uint8_t cigar_len = cigar_data.size();

    // Number of CIGAR elements
    total_size += sizeof(uint8_t);

    // Length and offset for each CIGAR element
    total_size += cigar_len * (sizeof(CountType));

    // Size of mdz_nucleotides vector
    total_size += sizeof(uint8_t);

    // MD:Z nucleotides
    total_size += mdz_nucleotides.size() * sizeof(uint8_t);

    return total_size;
}

std::vector<char> Seed::serialize() {

    // Total number of bytes after serialization
    uint16_t total_size = serialize_size();

    // Number of CIGAR elements
    uint8_t cigar_len = (uint8_t) cigar_data.size();

    // Char vector to store the data
    std::vector<char> data(total_size);
    char *d = data.data();

    // Write forward vertex descriptor
    memcpy(d, &fwd_vDesc, sizeof(FMVertexDescriptor));
    d += sizeof(FMVertexDescriptor);

    // Write reverse vertex descriptor
    memcpy(d, &rev_vDesc, sizeof(FMVertexDescriptor));
    d += sizeof(FMVertexDescriptor);

    // Write number of errors
    memcpy(d, &max_as, sizeof(ScoreType));
    d += sizeof(ScoreType);

    // Write number of CIGAR elements
    memcpy(d, &cigar_len, sizeof(uint8_t));
    d += sizeof(uint8_t);

    // Write the CIGAR elements themselves
    for (auto it = cigar_data.begin(); it != cigar_data.end(); ++it) {

        // Serialize the CIGAR elements information
        CountType serialized_value = it->length;
        serialized_value = (serialized_value << 3);
        serialized_value |= it->operation;

        memcpy(d, &(serialized_value), sizeof(CountType));
        d += sizeof(CountType);

    }

    // Write size of mdz_nucleotides vector
    memcpy(d, &mdz_length, sizeof(uint8_t));
    d += sizeof(uint8_t);

    // MD:Z nucleotides
    for (auto it = mdz_nucleotides.begin(); it != mdz_nucleotides.end(); ++it) {
        uint8_t next_byte = *it;

        memcpy(d, &next_byte, sizeof(uint8_t));
        d += sizeof(uint8_t);
    }

    return data;
}

uint16_t Seed::deserialize(char *d) {

    // Total number of bytes read
    uint16_t bytes = 0;

    // Forward FM-Index Vertex Descriptor
    memcpy(&fwd_vDesc, d + bytes, sizeof(FMVertexDescriptor));
    bytes += sizeof(FMVertexDescriptor);

    // Reverse FM-Index Vertex Descriptor
    memcpy(&rev_vDesc, d + bytes, sizeof(FMVertexDescriptor));
    bytes += sizeof(FMVertexDescriptor);

    // Number of errors
    memcpy(&max_as, d + bytes, sizeof(ScoreType));
    bytes += sizeof(ScoreType);

    // Number of CIGAR elements
    uint8_t cigar_len = 0;
    memcpy(&cigar_len, d + bytes, sizeof(uint8_t));
    bytes += sizeof(uint8_t);

    // The CIGAR elements themselves
    cigar_data.clear();
    for (uint8_t i = 0; i < cigar_len; ++i) {

        // Deserialize CIGAR element
        CountType serialized_value;
        memcpy(&(serialized_value), d + bytes, sizeof(CountType));
        bytes += sizeof(CountType);

        Operations operation = get_operation(serialized_value & CountType(three_bit_mask));
        CountType length = (serialized_value >> 3);

        // Create CigarElement
        CigarElement cig(length, operation);

        // Add to CIGAR vector
        cigar_data.emplace_back(cig);
    }

    // Read size of mdz_nucleotides vector
    memcpy(&mdz_length, d + bytes, sizeof(uint8_t));
    bytes += sizeof(uint8_t);

    // MD:Z nucleotides
    for (CountType i = 0; i < (mdz_length + 3) / 4; i++) {
        uint8_t next_byte;

        memcpy(&next_byte, d + bytes, sizeof(uint8_t));
        bytes += sizeof(uint8_t);

        mdz_nucleotides.push_back(next_byte);

    }

    return bytes;
}

ScoreType Seed::get_as() const {

    ScoreType as = 0;

    for (auto const &cigar : cigar_data) {

        if (cigar.length == 0)
            continue;

        switch (cigar.operation) {

            case SOFTCLIP:
                // Opening
                as -= this->settings->softclipOpeningPenalty;

                // Extension
                as -= cigar.length * this->settings->softclipExtensionPenalty;

                break;

            case NO_MATCH:
                as -= cigar.length * this->settings->mismatchPenalty;

                break;

            case INSERTION:

                // Opening
                as -= this->settings->insertionOpeningPenalty;

                // Extension
                as -= cigar.length * this->settings->insertionExtensionPenalty;

                break;

            case DELETION:

                // Opening
                as -= this->settings->deletionOpeningPenalty;

                // Extension
                as -= cigar.length * this->settings->deletionExtensionPenalty;

                break;

            default: // Match

                as += cigar.length * this->settings->matchScore;

                break;
        }
    }

    return as;
}

CountType Seed::get_nm() const {

    CountType nm = 0;

    // Don't count mismatches at front or end of the CIGAR string
    for (auto el = ++(cigar_data.begin()); el != cigar_data.end(); ++el)
        nm += el->operation != MATCH ? el->length : 0;

    return nm;
}

CountType Seed::get_softclip_length() const {
    if (cigar_data.size() == 0)
        return 0;

    CountType sc_length = cigar_data.front().operation == SOFTCLIP ? cigar_data.front().length : 0;
    return sc_length;
}

void Seed::add_mdz_nucleotide(char nucl) {
    uint8_t n = twobit_repr(nucl) << (6 - (2 * (mdz_length % 4)));

    if (mdz_length % 4 == 0)
        mdz_nucleotides.push_back(n);
    else
        mdz_nucleotides.back() = mdz_nucleotides.back() | n;

    mdz_length += 1;
}

void Seed::add_realigned_mdz_nucleotide(char nucl) {
    uint8_t n = twobit_repr(nucl) << 6;

    if (mdz_length == 0) {
        mdz_nucleotides.push_back(n);
    } else {
        for (int index = mdz_length - 1; index >= 0; index--) {
            if (index == mdz_length - 1 && mdz_length % 4 == 0) {
                uint8_t carriedBase = mdz_nucleotides[index] & 3;
                mdz_nucleotides.push_back(carriedBase);
            }
            if (index == 0) {
                mdz_nucleotides[index] = (mdz_nucleotides[index] >> 2) | n;
            } else {
                mdz_nucleotides[index] = ((mdz_nucleotides[index - 1] << 6) | (mdz_nucleotides[index] >> 2));
            }
        }
    }

    mdz_length += 1;
}

std::string Seed::getMDZString() const {
    std::string mdz_string = "";
    uint8_t nucleotide_pos = 0;
    auto mdz_it = mdz_nucleotides.begin();
    CountType match_counter = 0;

    for (auto el = cigar_data.begin(); el != cigar_data.end(); ++el) {
        switch (el->operation) {

            // Softclip not included in MD:Z
            case SOFTCLIP:
                continue;

            // DELETION or NO_MATCH: Add previous match length and nucleotides for the current region.
            case DELETION:
            case NO_MATCH:

                if (match_counter != 0)
                    mdz_string += std::to_string(match_counter);

                for (CountType i = 0; i < el->length; i++) {
                    mdz_string += revtwobit_repr(((*mdz_it) >> (6 - (2 * nucleotide_pos))) & 3);
                    if (nucleotide_pos >= 3) {
                        nucleotide_pos = 0;
                        ++mdz_it;
                    } else {
                        nucleotide_pos += 1;
                    }
                }

                match_counter = 0;
                break;

            // INSERTION: Do nothing.
            case INSERTION:
                break;

            // Match: Count the region length.
            default:
                match_counter += el->length;
                break;
        }
    }

    // Add last match sequence if exist
    if (match_counter != 0)
        mdz_string += std::to_string(match_counter);

    return mdz_string;
}

std::vector<GenomePosType>
Seed::getPositions(CountType firstPosition, CountType lastPosition, SerializableIndex *index) const {

    // Total number of positions covered by this seed
    CountType seed_positions = fwd_vDesc.range.i2 - fwd_vDesc.range.i1;

    // Invalid function parameters ==> return empty list
    if (lastPosition <= firstPosition || seed_positions <= firstPosition)
        return std::vector<GenomePosType>();

    // Modify the vertex descriptor of the seed according to the given function parameters
    FMVertexDescriptor sub_vDesc = fwd_vDesc;

    if (firstPosition > 0)
        sub_vDesc.range.i1 += firstPosition;

    if (lastPosition < seed_positions) // If lastPosition > seed_positions, use the original vDesc.range.i2
        sub_vDesc.range.i2 = fwd_vDesc.range.i1 + lastPosition;

    // FM iterator
    FMTopDownIterator it(*index->index);
    it.fwdIter.vDesc = sub_vDesc;
    it.revIter.vDesc = rev_vDesc;

    // List containing the positions obtained from the index (reserve enough space to not move the list in memory)
    std::vector<GenomePosType> position_list;

    // Get occurences
    auto positions = getOccurrences(it, seqan::Fwd());
    CountType sub_positions = length(positions);
    position_list.reserve(std::min(seed_positions, sub_positions));

    // Put positions into the return list
    for (CountType i = 0; i < sub_positions; ++i) {
        position_list.emplace_back(positions[i].i1, positions[i].i2);
    }

    return position_list;
}

uint64_t ReadAlignment::serialize_size() const {

    // Total size
    uint64_t total_size = 0;

    // Flag
    total_size += 1;

    // Cycle number
    total_size += sizeof(CountType);

    // Last invalid cycle
    total_size += sizeof(CountType);

    // Sequence length
    total_size += sizeof(CountType);

    // Barcode length
    total_size += sizeof(CountType);

    // The barcode sequence itself
    total_size += barcodeStoreVector.size() * (sizeof(uint8_t));

    // Number of seeds
    total_size += sizeof(uint32_t);

    // Size of the single seeds for each seed
    for (auto &s : seeds) {
        total_size += sizeof(uint16_t) + s->serialize_size();
    }

    return total_size;
}

std::vector<char> ReadAlignment::serialize() {

    // Total size
    uint64_t total_size = serialize_size();

    // Number of seeds
    uint32_t num_seeds = (uint32_t) seeds.size();

    // Char vector to store the data
    std::vector<char> data(total_size);
    char *d = data.data();

    // The flag
    memcpy(d, &flags, 1);
    d++;

    // Sequence length
    memcpy(d, &sequenceLen, sizeof(CountType));
    d += sizeof(CountType);

    // Barcode length
    memcpy(d, &barcodeLen, sizeof(CountType));
    d += sizeof(CountType);

    // Barcode sequence itself
    for (auto it = barcodeStoreVector.begin(); it != barcodeStoreVector.end(); ++it) {
        memcpy(d, &(*it), sizeof(uint8_t));
        d += sizeof(uint8_t);
    }

    // Number of seeds
    memcpy(d, &num_seeds, sizeof(uint32_t));
    d += sizeof(uint32_t);

    // The seeds themselves
    for (auto it = seeds.begin(); it != seeds.end(); ++it) {

        std::vector<char> seed_data = (*it)->serialize();
        uint16_t seed_size = seed_data.size();

        memcpy(d, &seed_size, sizeof(uint16_t));
        d += sizeof(uint16_t);

        memcpy(d, seed_data.data(), seed_size);
        d += seed_size;
    }

    return data;
}

uint64_t ReadAlignment::deserialize(char *d) {

    // Total number of bytes
    uint64_t bytes = 0;

    // The flag
    memcpy(&flags, d, 1);
    bytes++;

    // Sequence length
    sequenceLen = 0;
    memcpy(&sequenceLen, d + bytes, sizeof(CountType));
    bytes += sizeof(CountType);

    // Barcode length
    barcodeLen = 0;
    memcpy(&barcodeLen, d + bytes, sizeof(CountType));
    bytes += sizeof(CountType);

    // The barcode itself
    unsigned barVec_size = barcodeLen;
    barcodeStoreVector.clear();
    barcodeStoreVector.reserve(barVec_size);
    for (unsigned i = 0; i < barVec_size; ++i) {
        uint8_t elem;
        memcpy(&(elem), d + bytes, sizeof(uint8_t));
        bytes += sizeof(uint8_t);
        barcodeStoreVector.push_back(elem);
    }

    // Number of seeds
    uint32_t num_seeds = 0;
    memcpy(&num_seeds, d + bytes, sizeof(uint32_t));
    bytes += sizeof(uint32_t);

    // The seeds themselves
    seeds.clear();
    for (uint32_t i = 0; i < num_seeds; ++i) {

        uint16_t seed_size = 0;
        memcpy(&seed_size, d + bytes, sizeof(uint16_t));
        bytes += sizeof(uint16_t);

        std::vector<char> seed_data(seed_size, 0);
        memcpy(seed_data.data(), d + bytes, seed_size);
        bytes += seed_size;

        USeed s(new Seed(settings));
        s->deserialize(seed_data.data());

        // Must be sorted after extension anyways, so just push back
        seeds.push_back(std::move(s));
    }

    return bytes;
}

std::string ReadAlignment::getSequenceString() const {

    std::string seq = "";
    seq.reserve(sequenceLen);

    // iterate through all sequence bytes
    for (unsigned i = 0; i < sequenceLen; i++) {

        // Next basecall (6 bits quality; 2 bits nucleotide)
        uint8_t next = (*this->read)[i];

        if (next < 4) { // qual == 0 --> N-call
            seq.append("N");
        } else {        // qual > 0  --> write nucleotide
            seq += revtwobit_repr(next & two_bit_mask);
        }
    }

    // return barcode sequence
    return seq;
}

std::string ReadAlignment::getBarcodeString() const {

    std::string seq = "";
    seq.reserve(sequenceLen);

    // iterate through all sequence bytes
    for (unsigned i = 0; i < barcodeLen; i++) {

        // Next basecall (6 bits quality; 2 bits nucleotide)
        uint8_t next = barcodeStoreVector[i];

        if (next < 4) { // qual == 0 --> N-call
            seq.append("N");
        } else {        // qual > 0  --> write nucleotide
            seq += revtwobit_repr(next & two_bit_mask);
        }
    }

    // return barcode sequence
    return seq;
}

std::string ReadAlignment::getQualityString() const {

    std::string qual = "";
    qual.reserve(sequenceLen);

    // iterate through all sequence bytes
    for (unsigned i = 0; i < sequenceLen; i++) {

        // Next Quality (shift by 2 bit nucleotide information)
        uint8_t next_qual = (*this->read)[i] >> 2;

        qual += (to_phred_quality(next_qual));
    }

    // return PHRED quality sequence
    return qual;
}

void ReadAlignment::appendNucleotideToSequenceStoreVector(char bc, bool appendToBarCode) {
    if (appendToBarCode) {
        barcodeStoreVector.push_back(bc);
        barcodeLen++;
    } else sequenceLen++;
    return;
}

void ReadAlignment::extendSeed(char base, USeed origin, SeedVec &newSeeds, SerializableIndex *index) {

    uint64_t origin_range = origin->fwd_vDesc.range.i2 - origin->fwd_vDesc.range.i1;
    uint64_t handled_range = 0;

    // The new base / nucleotide
    CountType tbr = twobit_repr(base);

    // Handle match nucleotide
    {
        // Handle matching nucleotide first (this should be the default case)
        FMTopDownIterator it(*index->index);
        it.fwdIter.vDesc = origin->fwd_vDesc;
        it.revIter.vDesc = origin->rev_vDesc;
        if (goDown(it, seqan::DnaString(base), seqan::Fwd())) {
            getMatchSeeds(tbr, tbr, it, origin, newSeeds);
            handled_range += (it.fwdIter.vDesc.range.i2 - it.fwdIter.vDesc.range.i1);
        }
    }

    // If all index entries are matches, no mismatches or InDels have to be handled.
    if (handled_range >= origin_range)
        return;

    // Handle insertion
    getInsertionSeeds(origin, newSeeds);

    // Handle mismatches and deletions
    for (CountType index_base = 0; index_base < 4; index_base++) {

        // Stop if all index entries were handled
        if (handled_range >= origin_range)
            break;

        // Match nucleotide was already handled, so skip it
        if (index_base == tbr)
            continue;

        FMTopDownIterator it(*index->index);
        it.fwdIter.vDesc = origin->fwd_vDesc;
        it.revIter.vDesc = origin->rev_vDesc;

        if (goDown(it, seqan::DnaString(index_base), seqan::Fwd())) {
            // Handle no_match
            getMatchSeeds(tbr, index_base, it, origin, newSeeds);

            // Handle deletion
            getDeletionSeeds(tbr, index_base, it, origin, newSeeds, index);

            // Increase range of handled index entries
            handled_range += it.fwdIter.vDesc.range.i2 - it.fwdIter.vDesc.range.i1;
        }
    }
}

void ReadAlignment::realignMatchRegion(CountType read_base, CountType index_base, FMTopDownIterator &it,
                                       USeed origin, SeedVec &newSeeds) {
    ScoreType new_max_as = origin->max_as;

    // Ensure that there is a soft clip
    if (origin->cigar_data.front().operation != SOFTCLIP)
        return;

    if (read_base != index_base) { // NO_MATCH
        new_max_as -= (this->settings->mismatchPenalty + this->settings->matchScore);
    
        if (origin->cigar_data.at(1).operation == DELETION) return;
        if (origin->cigar_data.at(1).operation == INSERTION) return;
    }

    // If max_as is no longer valid, don't create related seeds
    if (new_max_as < getMinCycleScore(cycle, total_cycles))
        return;

    // Copy data from origin seed
    USeed s(new Seed(settings));
    s->fwd_vDesc = it.fwdIter.vDesc;
    s->rev_vDesc = it.revIter.vDesc;
    s->cigar_data = origin->cigar_data;
    s->mdz_nucleotides = origin->mdz_nucleotides;
    s->max_as = new_max_as;
    s->mdz_length = origin->mdz_length;

    // Insert NO_MATCH region if necessary
    if (read_base != index_base && s->cigar_data[1].operation != NO_MATCH) {
        // TODO: Find a better way than insert
        s->cigar_data.insert(s->cigar_data.begin() + 1, CigarElement(0, NO_MATCH));
    }
    // Insert MATCH region if necessary
    else if (read_base == index_base && s->cigar_data[1].operation != MATCH) {
        // TODO: Find a better way than insert
        s->cigar_data.insert(s->cigar_data.begin() + 1, CigarElement(0, MATCH));
    }

    s->cigar_data[1].length += 1; // Increase match/mismatch length
    s->cigar_data[0].length -= 1; // Decrease soft clip length

    if (s->cigar_data[0].length == 0) {
        s->cigar_data.erase(s->cigar_data.begin());
    }

    // Add nucleotide for MDZ tag
    if (read_base != index_base){
        s->add_realigned_mdz_nucleotide(revtwobit_repr(index_base));
    }

    newSeeds.emplace_back(s); // Push the new seed s to the vector
}

void ReadAlignment::getMatchSeeds(CountType read_base, CountType index_base, FMTopDownIterator &it, USeed origin,
                                  SeedVec &newSeeds) {

    ScoreType new_max_as = origin->max_as;

    if (read_base != index_base) { // NO_MATCH
        new_max_as -= (this->settings->mismatchPenalty + this->settings->matchScore);

        // DON'T FINISH DELETION AND INSERTION REGIONS BY NO_MATCH!!!
        if (origin->cigar_data.back().operation == DELETION)
            return;

        if (origin->cigar_data.back().operation == INSERTION)
            return;
    }

    // If max_as is no longer valid, don't create related seeds
    if (new_max_as < getMinCycleScore(cycle, total_cycles))
        return;

    // copy data from origin seed
    USeed s(new Seed(settings));
    s->fwd_vDesc = it.fwdIter.vDesc;
    s->rev_vDesc = it.revIter.vDesc;
    s->cigar_data = origin->cigar_data;
    s->mdz_nucleotides = origin->mdz_nucleotides;
    s->max_as = new_max_as;
    s->mdz_length = origin->mdz_length;

    // Insert NO_MATCH region if necessary
    if (read_base != index_base && s->cigar_data.back().operation != NO_MATCH)
        s->cigar_data.emplace_back(0, NO_MATCH);
    // Insert MATCH region if necessary
    else if (read_base == index_base && s->cigar_data.back().operation != MATCH)
        s->cigar_data.emplace_back(0, MATCH);

    s->cigar_data.back().length += 1; // Increase region length

    // Add nucleotide for MDZ tag
    if (read_base != index_base)
        s->add_mdz_nucleotide(revtwobit_repr(index_base));

    newSeeds.emplace_back(s); // Push the new seed to the vector
}

void ReadAlignment::realignInsertionRegion(USeed origin, SeedVec &newSeeds, bool lastBase) {

    // Ensure that there is a soft clip
    if (origin->cigar_data.front().operation != SOFTCLIP)
        return;

    // Don't start new insertion for last base
    if (lastBase)
        return;

    // Stop if no gaps are permitted
    if (this->settings->maxGapLength == 0)
        return;

    // Don't handle insertions after deletions
    if (origin->cigar_data[1].operation == DELETION)
        return;

    // Don't handle if insertion region is getting too long
    if (origin->cigar_data[1].operation == INSERTION && origin->cigar_data[1].length >= this->settings->maxGapLength)
        return;

    // Compute new maximal alignment score when having an insertion
    ScoreType new_max_as = origin->max_as - this->settings->insertionExtensionPenalty - this->settings->matchScore;
    if (origin->cigar_data[1].operation != INSERTION) {
        new_max_as -= this->settings->insertionOpeningPenalty;
    }

    // Extend insertion region as long as number of errors is allowed
    if (new_max_as >= getMinCycleScore(cycle, total_cycles)) {

        // Copy data from origin seed
        USeed s(new Seed(settings));
        s->fwd_vDesc = origin->fwd_vDesc; // Use origin since an insertion means to stay at the same index position
        s->rev_vDesc = origin->rev_vDesc;
        s->cigar_data = origin->cigar_data;
        s->mdz_length = origin->mdz_length;
        s->mdz_nucleotides = origin->mdz_nucleotides;
        s->max_as = new_max_as; // Increase number of errors

        // Insert INSERTION region if necessary
        if (s->cigar_data[1].operation != INSERTION) {
           s->cigar_data.insert(s->cigar_data.begin() + 1, CigarElement(0, INSERTION));
           // TODO: Find a better way than insert
        }

        s->cigar_data[1].length += 1; // Increase insertion length
        s->cigar_data[0].length -= 1; // Decrease soft clip length

        if (s->cigar_data[0].length == 0) {
            s->cigar_data.erase(s->cigar_data.begin());
        }

        newSeeds.emplace_back(s); // Push the new seed to the vector.
    }
}

void ReadAlignment::getInsertionSeeds(USeed origin, SeedVec &newSeeds) {

    // No Indels in the last cycle
    if (cycle == total_cycles)
        return;

    // Stop if no gaps are permitted
    if (this->settings->maxGapLength == 0)
        return;

    // Don't handle insertions after deletions.
    if (origin->cigar_data.back().operation == DELETION)
        return;

    // Don't handle if insertion region is getting too long
    if (origin->cigar_data.back().operation == INSERTION &&
        origin->cigar_data.back().length >= this->settings->maxGapLength)
        return;

    // Compute new maximal alignment score when having an insertion
    ScoreType new_max_as = origin->max_as - this->settings->insertionExtensionPenalty - this->settings->matchScore;
    if (origin->cigar_data.back().operation != INSERTION)
        new_max_as -= this->settings->insertionOpeningPenalty;

    // Extend insertion region as long as number of errors is allowed.
    if (new_max_as >= getMinCycleScore(cycle, total_cycles)) {

        // copy data from origin seed
        USeed s(new Seed(settings));
        s->fwd_vDesc = origin->fwd_vDesc; // Use origin since an insertion means to stay at the same index position
        s->rev_vDesc = origin->rev_vDesc;
        s->cigar_data = origin->cigar_data;
        s->mdz_length = origin->mdz_length;
        s->mdz_nucleotides = origin->mdz_nucleotides;
        s->max_as = new_max_as; // Increase number of errors

        // Insert INSERTION region if necessary
        if (s->cigar_data.back().operation != INSERTION)
            s->cigar_data.emplace_back(0, INSERTION);

        s->cigar_data.back().length += 1; // Increase insertion length

        newSeeds.emplace_back(s); // Push the new seed to the vector.
    }
}

void ReadAlignment::realignDeletionRegion(CountType read_base, CountType index_base, FMTopDownIterator &it,
                                          USeed origin, SeedVec &newSeeds, SerializableIndex *index) {
    // Ensure that there is a soft clip
    if (origin->cigar_data.front().operation != SOFTCLIP)
        return;

    // Stop if no gaps are permitted
    if (this->settings->maxGapLength == 0)
        return;

    // Don't handle nucleotide matches
    if (read_base == index_base)
        return;

    // Don't handle insertion regions
    if (origin->cigar_data[1].operation == INSERTION)
        return;

    // Can only be opening (extension is performed in recursiveGoDownForRealignment() function)
    ScoreType new_max_as =
            origin->max_as - this->settings->deletionOpeningPenalty - this->settings->deletionExtensionPenalty;

    // Don't start iteration when having already all allowed errors.
    if (new_max_as < getMinCycleScore(cycle, total_cycles))
        return;

    // Copy data from origin seed
    USeed s(new Seed(settings));
    s->fwd_vDesc = it.fwdIter.vDesc;
    s->rev_vDesc = it.revIter.vDesc;
    s->cigar_data = origin->cigar_data;
    s->mdz_length = origin->mdz_length;
    s->mdz_nucleotides = origin->mdz_nucleotides;
    s->max_as = new_max_as; // Increase number of errors

    // Init deletion region
    s->cigar_data.insert(s->cigar_data.begin() + 1, CigarElement(1, DELETION)); // TODO: Find a better way than insert

    // Add MDZ nucleotide
    s->add_realigned_mdz_nucleotide(revtwobit_repr(index_base));

    recursiveGoDownForRealignment(read_base, s, newSeeds, index);
}

void ReadAlignment::getDeletionSeeds(CountType read_base, CountType index_base, FMTopDownIterator &it, USeed origin,
                                     SeedVec &newSeeds, SerializableIndex *index) {

    // No Indels in the last cycle
    if (cycle == total_cycles)
        return;

    // Stop if no gaps are permitted
    if (this->settings->maxGapLength == 0)
        return;

    // Don't handle nucleotide matches
    if (read_base == index_base)
        return;

    // Don't handle insertion regions
    if (origin->cigar_data.back().operation == INSERTION)
        return;

    // Can only be opening (extension is performed in recursive_goDown() function)
    ScoreType new_max_as =
            origin->max_as - this->settings->deletionOpeningPenalty - this->settings->deletionExtensionPenalty;

    // Don't start iteration when having already all allowed errors.
    if (new_max_as < getMinCycleScore(cycle, total_cycles))
        return;

    // copy data from origin seed
    USeed s(new Seed(settings));
    s->fwd_vDesc = it.fwdIter.vDesc;
    s->rev_vDesc = it.revIter.vDesc;
    s->cigar_data = origin->cigar_data;
    s->mdz_length = origin->mdz_length;
    s->mdz_nucleotides = origin->mdz_nucleotides;
    s->max_as = new_max_as; // Increase number of errors

    s->cigar_data.emplace_back(1, DELETION); // init deletion region

    // Add MDZ nucleotide
    s->add_mdz_nucleotide(revtwobit_repr(index_base));

    recursive_goDown(read_base, s, newSeeds, index);
}

void ReadAlignment::recursiveGoDownForRealignment(CountType base_repr, USeed origin, SeedVec &newSeeds,
                                                  SerializableIndex *index) {

    // Count ranges to avoid unnecessary index calls
    uint64_t origin_range = origin->rev_vDesc.range.i2 - origin->rev_vDesc.range.i1;
    uint64_t handled_range = 0;

    if (origin->max_as < this->settings->minAs) // Too many errors -> no seeds
        return;

    // Only handle deletion regions
    if (origin->cigar_data[1].operation != DELETION)
        return;

    // Start with match base
    {
        // Create index iterator
        FMTopDownIterator it(*index->index);
        it.fwdIter.vDesc = origin->fwd_vDesc;
        it.revIter.vDesc = origin->rev_vDesc;

        // Only consider paths existing in the index
        if (goDown(it, seqan::DnaString(revtwobit_repr(base_repr)), seqan::Rev())) {

            // Copy data from origin seed
            USeed s(new Seed(settings));
            s->fwd_vDesc = it.fwdIter.vDesc;
            s->rev_vDesc = it.revIter.vDesc;
            s->cigar_data = origin->cigar_data;
            s->mdz_length = origin->mdz_length;
            s->mdz_nucleotides = origin->mdz_nucleotides;
            s->max_as = origin->max_as;

            // Init MATCH region
            s->cigar_data.insert(s->cigar_data.begin() + 1, CigarElement(1, MATCH));
            // TODO: Find a better way than insert

            // Decrease soft clip region here because deletions have to be terminated by a match
            s->cigar_data[0].length -= 1;

            if (s->cigar_data[0].length == 0) {
                s->cigar_data.erase(s->cigar_data.begin());
            }

            newSeeds.emplace_back(s);

            handled_range += it.revIter.vDesc.range.i2 - it.revIter.vDesc.range.i1;
        }
    }

    if (origin->cigar_data[1].length >= this->settings->maxGapLength)
        return;

    ScoreType new_max_as = origin->max_as - this->settings->deletionExtensionPenalty;

    // Stop if maximum number of errors reached
    if (new_max_as < getMinCycleScore(cycle, total_cycles))
        return;

    // Iterate through all non-match bases
    for (int b = 0; b < 4; b++) {

        // Skip the match base which was already handled
        if (b == base_repr)
            continue;

        // Stop if all bases occuring in the index were already handled
        if (handled_range >= origin_range)
            break;

        // Create index iterator
        FMTopDownIterator it(*index->index);
        it.fwdIter.vDesc = origin->fwd_vDesc;
        it.revIter.vDesc = origin->rev_vDesc;

        // Only consider paths existing in the index
        if (goDown(it, seqan::DnaString(revtwobit_repr(b)), seqan::Rev())) {

            // Copy data from origin seed
            USeed s(new Seed(settings));
            s->fwd_vDesc = it.fwdIter.vDesc;
            s->rev_vDesc = it.revIter.vDesc;
            s->cigar_data = origin->cigar_data;
            s->mdz_length = origin->mdz_length;
            s->mdz_nucleotides = origin->mdz_nucleotides;
            s->max_as = new_max_as; // Increase number of errors
            s->cigar_data[1].length += 1; // Extend deletion region
            s->add_realigned_mdz_nucleotide(revtwobit_repr(b));

            recursiveGoDownForRealignment(base_repr, s, newSeeds, index);

            handled_range += (it.revIter.vDesc.range.i2 - it.revIter.vDesc.range.i1);
        }
    }
}

void ReadAlignment::recursive_goDown(CountType base_repr, USeed origin, SeedVec &newSeeds, SerializableIndex *index) {

    // Count ranges to avoid unnecessary index calls
    uint64_t origin_range = origin->fwd_vDesc.range.i2 - origin->fwd_vDesc.range.i1;
    uint64_t handled_range = 0;

    if (origin->max_as < this->settings->minAs) // too many errors -> no seeds
        return;

    // Only handle deletion regions
    if (origin->cigar_data.back().operation != DELETION)
        return;

    // Start with match base
    {
        // Create index iterator
        FMTopDownIterator it(*index->index);
        it.fwdIter.vDesc = origin->fwd_vDesc;
        it.revIter.vDesc = origin->rev_vDesc;

        // Only consider paths existing in the index
        if (goDown(it, seqan::DnaString(revtwobit_repr(base_repr)), seqan::Fwd())) {

            // copy data from origin seed
            USeed s(new Seed(settings));
            s->fwd_vDesc = it.fwdIter.vDesc;
            s->rev_vDesc = it.revIter.vDesc;
            s->cigar_data = origin->cigar_data;
            s->mdz_length = origin->mdz_length;
            s->mdz_nucleotides = origin->mdz_nucleotides;
            s->max_as = origin->max_as;

            s->cigar_data.emplace_back(1, MATCH); // init MATCH region
            newSeeds.emplace_back(s);

            handled_range += it.fwdIter.vDesc.range.i2 - it.fwdIter.vDesc.range.i1;
        }
    }

    if (origin->cigar_data.back().length >= this->settings->maxGapLength)
        return;

    ScoreType new_max_as = origin->max_as - this->settings->deletionExtensionPenalty;

    // stop if maximum number of errors reached
    if (new_max_as < getMinCycleScore(cycle, total_cycles))
        return;

    // iterate through all non-match bases
    for (int b = 0; b < 4; b++) {

        // Skip the match base which was already handled
        if (b == base_repr)
            continue;

        // Stop if all bases occuring in the index were already handled
        if (handled_range >= origin_range)
            break;

        // Create index iterator
        FMTopDownIterator it(*index->index);
        it.fwdIter.vDesc = origin->fwd_vDesc;
        it.revIter.vDesc = origin->rev_vDesc;

        // Only consider paths existing in the index
        if (goDown(it, seqan::DnaString(revtwobit_repr(b)), seqan::Fwd())) {

            // copy data from origin seed
            USeed s(new Seed(settings));
            s->fwd_vDesc = it.fwdIter.vDesc;
            s->rev_vDesc = it.revIter.vDesc;
            s->cigar_data = origin->cigar_data;
            s->mdz_length = origin->mdz_length;
            s->mdz_nucleotides = origin->mdz_nucleotides;
            s->max_as = new_max_as; // Increase number of errors
            s->cigar_data.back().length += 1; // extend deletion region
            s->add_mdz_nucleotide(revtwobit_repr(b));

            recursive_goDown(base_repr, s, newSeeds, index);

            handled_range += (it.fwdIter.vDesc.range.i2 - it.fwdIter.vDesc.range.i1);
        }
    }
}

void ReadAlignment::createSeeds(SeedVec &newSeeds, SerializableIndex *index) {

    CountType softclip_cycles = cycle - this->settings->anchorLength;
    CountType max_match_cycles = total_cycles - softclip_cycles;
    ScoreType max_as = (max_match_cycles * this->settings->matchScore) - getMinSoftclipPenalty(softclip_cycles);

    if (max_as < getMinCycleScore(cycle, total_cycles))
        return;

    std::string anchor_seq = getSequenceString().substr(sequenceLen - this->settings->anchorLength,
                                                        this->settings->anchorLength);

    FMTopDownIterator it(*index->index);

    if (goDown(it, seqan::DnaString(anchor_seq), seqan::Fwd())) {

        USeed seed(new Seed(settings));
        seed->max_as = max_as;
        if (cycle != this->settings->anchorLength)
            seed->cigar_data.emplace_back((cycle - this->settings->anchorLength), SOFTCLIP); // Add softclip
        seed->cigar_data.emplace_back(this->settings->anchorLength, MATCH);
        seed->fwd_vDesc = it.fwdIter.vDesc;
        seed->rev_vDesc = it.revIter.vDesc;

        newSeeds.emplace_back(seed);
    }
}

void ReadAlignment::extend_alignment(char bc, SerializableIndex *index, int &executedRealignments, int &seedsWithSoftclips) {

    // Cycle is not allowed to be > total_cycles
    assert(total_cycles >= cycle);

    appendNucleotideToSequenceStoreVector(bc);

    // Do not update the alignments when reading the first kmer_span-1 cycles
    if (cycle < this->settings->anchorLength) {
        return;
    }

    SeedVec newSeeds;

    // Reserve space for the worst case scenario: 4*M + 4*D + 1*I for each existing seed plus 1 new seed
    newSeeds.reserve(9 * seeds.size() + 1);

    // Extend existing seeds
    char base = revtwobit_repr(bc & 3);
    for (auto seed = seeds.begin(); seed != seeds.end(); ++seed) {
        extendSeed(base, (*seed), newSeeds, index);
    }

    // Create new seeds in defined intervals (intervals are handled inside createSeeds() function)
    if (isSeedingCycle(cycle)) {
        createSeeds(newSeeds, index);
    }

    // Move the new seeds to the seeds vector
    seeds = std::move(newSeeds);

    // Execute realignment of softclip in last cycle if it is enabled in the config file
    if (cycle == total_cycles && this->settings->resolveSoftclips) { // TODO: Maybe change starting point of realignment
        resolveSoftclip(index, executedRealignments);
    }

    // Nothing to sort or erase
    if (seeds.size() == 0) {
        return;
    } else if (seeds.size() == 1) {
        if ((*seeds.begin())->get_softclip_length() > 0) {
            seedsWithSoftclips++;
        }
        return;
    }

    // Sort the seeds by their vDesc positions (secondary: score)
    std::sort(seeds.begin(), seeds.end(), PCompFwd<USeed>);

    // From here, erase seeds that have the same vDesc positions but a lower or similar score than a previous one
    bool first_seed = true;
    auto last_vDesc = seeds.front()->fwd_vDesc;

    seeds.erase(std::remove_if(std::begin(seeds), std::end(seeds), [&](const USeed seed) mutable {

        if (seed->get_softclip_length() > 0) {
            seedsWithSoftclips++;
        }

        // Don't erase the first seed
        if (first_seed) {
            first_seed = false;
            return false;
        }

        // Erase all "duplicates", which means the same vDesc position but a lower or similar score than the previous one.
        bool is_duplicate = seed->fwd_vDesc == last_vDesc;
        last_vDesc = seed->fwd_vDesc;
        return is_duplicate;
    }), std::end(seeds));
}

void ReadAlignment::resolveSoftclip(SerializableIndex *index, int &executedRealignments) {

    SeedVec newSeeds;
    bool hasSoftclips = true;
    CountType iteration = 0;

    while (hasSoftclips) {
        iteration++;
        hasSoftclips = false;
        newSeeds.reserve(9 * seeds.size() + 1);

        for (auto seed = seeds.begin(); seed != seeds.end(); ++seed) {

            if ((*seed)->get_softclip_length() == 0) {
                newSeeds.push_back(*seed);
                continue;
            } else if ((*seed)->get_softclip_length() > 1) {
                hasSoftclips = true;
            }

            // Avoid penalizing region twice (for mistakes found after realignment and for softclip found before realignment)
            if (iteration == 1)
                (*seed)->max_as += this->settings->matchScore * (*seed)->get_softclip_length() + getMinSoftclipPenalty((*seed)->get_softclip_length());

            executedRealignments++;
            realignSeed((*seed), newSeeds, index);
        }

        seeds = std::move(newSeeds);
        newSeeds.clear();

        if (iteration%1 == 0) {
            // Nothing to sort or erase
            if (seeds.size() <= 1) {
                continue;
            }

            // Sort the seeds by their vDesc positions (secondary: score)
            std::sort(seeds.begin(), seeds.end(), PCompRev<USeed>);

            // From here, erase seeds that have the same vDesc positions but a lower or similar score than a previous one
            bool first_seed = true;
            auto last_vDesc = seeds.front()->rev_vDesc;

            seeds.erase(std::remove_if(std::begin(seeds), std::end(seeds), [&](const USeed seed) mutable {

                // Don't erase the first seed
                if (first_seed) {
                    first_seed = false;
                    return false;
                }

                // Erase all "duplicates", which means the same vDesc position but a lower or similar score than the previous one.
                bool is_duplicate = seed->rev_vDesc == last_vDesc;
                last_vDesc = seed->rev_vDesc;
                return is_duplicate;
            }), std::end(seeds));
        }
    }

    /*
    // Reserve space for the worst case scenario: 4*M + 4*D + 1*I for each existing seed plus 1 new seed
    SeedVec newSeeds;
    newSeeds.reserve(9 * seeds.size() + 1);

    for (auto seed = seeds.begin(); seed != seeds.end(); ++seed) {

        if ( (*seed)->get_softclip_length() == 0 ) continue;

        // Avoid penalizing region twice (for mistakes found after realignment and for softclip found before realignment)
        (*seed)->max_as += this->settings->matchScore * (*seed)->get_softclip_length() + getMinSoftclipPenalty((*seed)->get_softclip_length());

        executedRealignments++;
        realignRecursively((*seed), newSeeds, index);
    }

    // Move the new seeds to the seeds vector
    seeds = std::move(newSeeds);
    */
}

void ReadAlignment::realignSeed(USeed origin, SeedVec &newSeeds, SerializableIndex *index) {

    // Check if there is a front soft clip
    if (origin->cigar_data.front().operation == SOFTCLIP) {

        // Extract BaseCalls
        string sequence = getSequenceString();
        int currentSoftclipPosition = origin->get_softclip_length() - 1;
        bool isLastBase = currentSoftclipPosition == 0;

        uint64_t origin_range = origin->rev_vDesc.range.i2 - origin->rev_vDesc.range.i1;
        uint64_t handled_range = 0;

        // The new base
        CountType tbr = twobit_repr(sequence[currentSoftclipPosition]);

        // Handle matching nucleotide first (this should be the default case)
        FMTopDownIterator it(*index->index);
        it.fwdIter.vDesc = origin->fwd_vDesc;
        it.revIter.vDesc = origin->rev_vDesc;

        if (goDown(it, seqan::DnaString(sequence[currentSoftclipPosition]), seqan::Rev())) {
            realignMatchRegion(tbr, tbr, it, origin, newSeeds);
            handled_range += (it.revIter.vDesc.range.i2 - it.revIter.vDesc.range.i1);
        }

        // If not all index entries are matches, mismatches and InDels have to be handled
        if (handled_range < origin_range) {

            // Handle insertion
            realignInsertionRegion(origin, newSeeds, isLastBase);

            // Handle mismatches and deletions
            for (CountType index_base = 0; index_base < 4; index_base++) {

                // Stop if all index entries were handled
                if (handled_range >= origin_range)
                    break;

                // Match nucleotide was already handled, so skip it
                if (index_base == tbr)
                    continue;

                FMTopDownIterator it(*index->index);
                it.fwdIter.vDesc = origin->fwd_vDesc;
                it.revIter.vDesc = origin->rev_vDesc;

                if (goDown(it, seqan::DnaString(index_base), seqan::Rev())) {
                    // Handle mismatch
                    realignMatchRegion(tbr, index_base, it, origin, newSeeds);

                    // Handle deletion
                    realignDeletionRegion(tbr, index_base, it, origin, newSeeds, index);

                    // Increase range of handled index entries
                    handled_range += it.revIter.vDesc.range.i2 - it.revIter.vDesc.range.i1;
                }
            }
        }

        // Stop if we reached last base of softclip
        if (isLastBase)
            return;

        // Reserve space for the worst case scenario: 4*M + 4*D + 1*I for each existing seed plus 1 new seed
        //SeedVec recursiveNewSeeds;
        //recursiveNewSeeds.reserve(9 * newSeeds.size() + 1);

        //for (auto seed = newSeeds.begin(); seed != newSeeds.end(); ++seed) {
        //    realignSeed((*seed), recursiveNewSeeds, index);
        //}

        // Write recursiveNewSeeds to newSeeds for next iteration
        //newSeeds.clear();
        //newSeeds = std::move(recursiveNewSeeds);

    } else {
        newSeeds.push_back(origin);
    }
}

void ReadAlignment::setSettings(ReadAlignmentSettings *readAlignmentSettings) {
    this->settings = readAlignmentSettings;
}

void ReadAlignment::setRead(Read *readPointer) {
    this->read = readPointer;
}

CountType ReadAlignment::getBarcodeIndex() const {
    // Get the barcodes of the read
    std::string read_bc = getBarcodeString();

    if (read_bc.length() == 0)
        return UNDETERMINED;

    uint16_t fragment_errors = 0;
    uint16_t fragment_pos = 0;
    uint16_t fragment_num = 0;
    CountType matching_bc = UNDETERMINED;

    vector<vector<string>> barcodeVector = this->settings->barcodeVector;

    // Iterate through all user-defined (multi-)barcodes
    // That's quite complicated since the read barcodes are consecutive and the user barcodes are divided in vectors. // TODO: Change that?
    for (uint16_t barcodeIndex = 0; barcodeIndex < barcodeVector.size(); barcodeIndex++) {

        // Reset values for the barcode
        fragment_errors = 0;
        fragment_pos = 0;
        fragment_num = 0;
        matching_bc = barcodeIndex;

        // For each base of the read barcode
        for (uint16_t nucl = 0; nucl < read_bc.length(); nucl++) {

            // Reset values for each barcode fragment
            if (fragment_pos >= (barcodeVector[barcodeIndex])[fragment_num].length()) {
                fragment_pos = 0;
                fragment_num += 1;
                fragment_errors = 0;
                assert(fragment_num < (barcodeVector[barcodeIndex]).size());
            }

            // Compare nucleotides and increase the number of fragment errors if not equal
            if (read_bc.at(nucl) != (barcodeVector[barcodeIndex])[fragment_num].at(fragment_pos)) {
                fragment_errors++;
            }

            // If too many errors in a fragment, break the loop for the barcode
            if (fragment_errors > this->settings->barcodeErrors[fragment_num]) {
                matching_bc = UNDETERMINED;
                break;
            }

            fragment_pos += 1; // increment the fragment position

        }

        // If one barcode fulfilled the criteria, we can stop.
        if (matching_bc != UNDETERMINED)
            break;
    }

    return matching_bc;
}

void ReadAlignment::disable() {
    seeds.clear();
    flags = 0;
    if (!this->settings->keepAllSequences) {
        sequenceLen = 0;
    }
}

bool ReadAlignment::is_disabled() const {
    return flags == 0;
}

PositionType ReadAlignment::get_SAM_start_pos(GenomePosType p, USeed &sd, SerializableIndex *index) const {

    // Only valid if CIGAR string exist (should always be the case)
    if (sd->cigar_data.size() == 0)
        return std::numeric_limits<PositionType>::max();

    // Retrieve position from the index
    PositionType sam_pos = p.pos;

    // REVERSE POSITIONS: position is already correct
    if (index->isReverse(p.gid)) {
        return sam_pos;
    }

    // FORWARD POSITIONS: Compute start position
    else {
        sam_pos = index->getSequenceLength(p.gid) - p.pos;

        CountType cigLen = 0;
        for (auto el = sd->cigar_data.begin(); el != sd->cigar_data.end(); ++el) {
            if (el->operation != INSERTION) {
                cigLen += el->length;
            }
        }
        sam_pos -= cigLen;

        // Begin pos in SAM specification ignores the softclip.
        sam_pos += sd->get_softclip_length();
    }

    return sam_pos;
}

void ReadAlignment::sort_seeds_by_as() {
    std::sort(seeds.begin(), seeds.end(), seed_comparison_by_as);
}

std::vector<uint8_t> ReadAlignment::getMAPQs() const {

    // Vector that contains the final MAPQ values
    std::vector<uint8_t> mapqs;

    // True, if there is exactly one alignment for the read.
    bool unique = true;

    // Stop if not seeds.
    if (seeds.size() == 0)
        return mapqs;
    // Check if the best alignment can still be unique.
    else if (seeds.size() != 1)
        unique = false;

    // Vector that contains the MAPQ factor for each seed
    // The MAPQ factor is the weight of all alignments of a read
    std::vector<float> mapqFactors;

    uint16_t minSingleErrorPenalty = this->getMinSingleErrorPenalty();
    ScoreType maxPossibleScore = this->getMaxPossibleScore(cycle);

    // Maximal number of single errors for the minimal score of the current cycle (this does not consider affine gap penalties)
    float maxErrorsWithMinPenalty =
            float(maxPossibleScore - this->getMinCycleScore(cycle, total_cycles)) / float(minSingleErrorPenalty);

    // Maximal error percentage for the minimal score of the current cycle ( this does not consider affine gap penalties)
    float max_error_percent = float(100.0f * float(maxErrorsWithMinPenalty)) / float(total_cycles);

    mapqs.reserve(seeds.size());
    mapqFactors.reserve(seeds.size());

    // Sum of all MAPQ factors
    float mapqFactorsSum = 0;

    // Minimal number of errors for the best seed. This is required to compute the MAPQ factor
    float minReadErrors = float(getMaxPossibleScore(cycle) - seeds[0]->get_as()) / getMinSingleErrorPenalty();

    // Compute the sum of all MAPQ factors
    for (auto &s : seeds) {

        // Weight the minimal number of errors
        float mapqFactor = std::pow(0.01f,
                                    (float(getMaxPossibleScore(cycle) - s->get_as()) / getMinSingleErrorPenalty()) -
                                    minReadErrors);

        mapqFactors.push_back(mapqFactor);

        // For the sum, multiply the factor by the number of positions for this seed.
        mapqFactorsSum += (mapqFactor * (s->fwd_vDesc.range.i2 - s->fwd_vDesc.range.i1));

        // Check if the best alignment can still be unique
        if (s->fwd_vDesc.range.i2 - s->fwd_vDesc.range.i1 > 1)
            unique = false;
    }

    // Compute the MAPQ for all seeds
    for (CountType i = 0; i < seeds.size(); i++) {

        // Always give 42 for unique, perfectly matching alignments
        if (unique && seeds[i]->get_as() == maxPossibleScore) {
            mapqs.push_back(42);
        }

            // Otherwise, apply MAPQ heuristics. Base Call Quality is not considered.
        else {

            float error_percent =
                    float(100.0f * float(maxPossibleScore - seeds[i]->get_as()) / float(minSingleErrorPenalty)) /
                    float(cycle);

            float prob = 1.0f;
            if (seeds[i]->get_as() != maxPossibleScore) {
                prob = 0.49995f + (0.5f * (1.0f - std::pow(10.0f, -5.0f - max_error_percent + (2.0f * error_percent))));
            }

            mapqs.push_back(prob2mapq(prob * mapqFactors[i] / mapqFactorsSum));
        }
    }

    return mapqs;
}

void ReadAlignment::addReadInfoToRecord(seqan::BamAlignmentRecord &record) const {
    record.seq = getSequenceString();
    record.qual = getQualityString();
}

uint16_t ReadAlignment::getMinSingleErrorPenalty() const {

    // Mismatch: +1 mismatch, -1 match
    uint16_t mismatch_penalty = this->settings->mismatchPenalty + this->settings->matchScore;

    // Deletion: +1 deletion, maximum number of matches can still be reached
    uint16_t deletion_penalty = this->settings->deletionOpeningPenalty + this->settings->deletionExtensionPenalty;

    // Insertion: +1 insertion, -1 match
    uint16_t insertion_penalty = this->settings->insertionOpeningPenalty + this->settings->insertionExtensionPenalty +
                                 this->settings->matchScore;

    return std::min(mismatch_penalty, std::min(insertion_penalty, deletion_penalty));
}

int16_t ReadAlignment::getMinCycleScore(CountType cycle, CountType read_length) const {

    if (cycle < this->settings->anchorLength)
        return this->settings->minAs;

    uint16_t maxScore = getMaxPossibleScore(read_length);
    ScoreType minCycleScore = maxScore -
                              (ceil((cycle - this->settings->anchorLength) / float(this->settings->errorRate)) *
                               getMinSingleErrorPenalty());
    return std::max(minCycleScore, this->settings->minAs);
}

uint16_t ReadAlignment::getMaxPossibleScore(CountType cycles) const {
    return cycles * this->settings->matchScore;
}

uint16_t ReadAlignment::getMinSoftclipPenalty(CountType softclip_length) const {
    return ceil(float(softclip_length) / this->settings->anchorLength) * getMinSingleErrorPenalty();
}

bool ReadAlignment::isSeedingCycle(CountType cycle) {

    // Don't seed cycles smaller than the anchor length
    if (cycle < this->settings->anchorLength)
        return false;

    // Don't seed cycles if the maximal softclip length would be exceeded
    if ((cycle - this->settings->anchorLength) > this->settings->maxSoftclipLength)
        return false;

    // Create seeds when reaching the anchor length for the first time
    if (cycle == this->settings->anchorLength)
        return true;

    // Create seeds every seeding_interval cycle after the first anchor
    if ((cycle - this->settings->anchorLength) % this->settings->seedingInterval == 0)
        return true;

    return false;
}

ReadAlignmentSettings ReadAlignment::generateSettingsfromConfigurable(Configurable *configurable) {
    return {
            .anchorLength = configurable->getConfigEntry<uint16_t>("anchorLength"),
            .minAs = configurable->getConfigEntry<int16_t>("minAs"),
            .errorRate = configurable->getConfigEntry<uint16_t>("errorRate"),
            .mismatchPenalty = configurable->getConfigEntry<uint16_t>("mismatchPenalty"),
            .deletionOpeningPenalty = configurable->getConfigEntry<uint16_t>("deletionOpeningPenalty"),
            .deletionExtensionPenalty = configurable->getConfigEntry<uint16_t>("deletionExtensionPenalty"),
            .insertionOpeningPenalty = configurable->getConfigEntry<uint16_t>("insertionOpeningPenalty"),
            .insertionExtensionPenalty = configurable->getConfigEntry<uint16_t>("insertionExtensionPenalty"),
            .softclipOpeningPenalty = configurable->getConfigEntry<float>("softclipOpeningPenalty"),
            .softclipExtensionPenalty = configurable->getConfigEntry<float>("softclipExtensionPenalty"),
            .matchScore = configurable->getConfigEntry<uint16_t>("matchScore"),
            .maxSoftclipLength = configurable->getConfigEntry<uint16_t>("maxSoftclipLength"),
            .seedingInterval = configurable->getConfigEntry<uint16_t>("seedingInterval"),
            .maxGapLength = configurable->getConfigEntry<uint16_t>("maxGapLength"),
            .keepAllSequences = configurable->getConfigEntry<bool>("keepAllSequences"),
            .extendedCigar = configurable->getConfigEntry<bool>("extendedCigar"),
            .barcodeErrors = configurable->getConfigEntry<vector<uint16_t>>("barcodeErrors"),
            .barcodeVector = configurable->getConfigEntry<vector<vector<string>>>("barcodeVector"),
            .resolveSoftclips = configurable->getConfigEntry<bool>("resolveSoftclips")

    };
}
