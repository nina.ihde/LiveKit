#ifndef KINDEX_H
#define KINDEX_H

#include "headers.h"
#include "definitions.h"
#include "global_variables.h"
#include "tools.h"
#include <seqan/sequence.h>
#include <seqan/index.h>

/**
 * FM-Index Config.
 */
typedef seqan::FastFMIndexConfig<void, uint64_t, 2, 1> FMIConfig;

/**
 * FM-Index data type.
 */
typedef seqan::Index<seqan::StringSet<seqan::DnaString>, seqan::FMIndex<void, FMIConfig> > FMIndex;

/**
 * Bidirectional FM-Index data type.
 */
typedef seqan::Index<seqan::StringSet<seqan::DnaString>, seqan::BidirectionalIndex<seqan::FMIndex<void, FMIConfig> >> BidirectionalFMIndex;

/**
 * Iterator to go through the bidirectional FM-Index structure.
 */
typedef seqan::Iter<BidirectionalFMIndex, seqan::VSTree<seqan::TopDown<seqan::ParentLinks<>>>> FMTopDownIterator;

/**
 * Data type to find the node stored in the FM-Index.
 */
typedef seqan::Iter<FMIndex, seqan::VSTree<seqan::TopDown<seqan::Preorder>>>::TVertexDesc FMVertexDescriptor;

inline bool operator==(const FMVertexDescriptor l, FMVertexDescriptor r) {
    return l.range == r.range;
}

inline bool operator<(const FMVertexDescriptor l, FMVertexDescriptor r) {
    if (l.range.i1 == r.range.i1)
        return l.range.i2 < r.range.i2;
    return l.range.i1 < r.range.i1;
}

inline bool operator>(const FMVertexDescriptor l, FMVertexDescriptor r) {
    if (l.range.i1 == r.range.i1)
        return l.range.i2 > r.range.i2;
    return l.range.i1 > r.range.i1;
}

inline bool operator!=(const FMVertexDescriptor l, FMVertexDescriptor r) {
    return !(l == r);
}

#endif /* KINDEX_H */
