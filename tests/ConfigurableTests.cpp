#include "./catch2/catch.hpp"
#include "./fakeit/fakeit.hpp"

#include "../framework/configuration/Configurable.h"

#include <algorithm>
#include <boost/algorithm/string/classification.hpp>
#include <boost/algorithm/string/split.hpp>

using namespace std;

class ConcreteConfigurable : public Configurable {
public:
    void setConfig() override {
        this->registerConfigEntry<int>("integerParameter", 99);
        this->registerConfigEntry<string>("stringParameter", "defaultValue");
        this->registerConfigEntry<bool>("boolParameter", false);

        function<vector<int>(int)> converter = [](int input) {
            vector<int> result;

            while (input) {
                result.push_back(input % 10);
                input /= 10;
            }

            reverse(result.begin(), result.end());
            return result;
        };
        this->registerConfigEntry<vector<int>, int>("complexType", 1101, converter);


        this->registerConfigEntry<vector<pair<int, char>>, string>("readsParameter", "100R,8B,8B,100R",
           [](string input) {
               vector<pair<int, char>> result;
               vector<string> intermediateResult;
               boost::split(intermediateResult, input,
                            boost::is_any_of(","),
                            boost::token_compress_on);
               for (auto el : intermediateResult) {
                   result.push_back(make_pair(
                           stoi(el.substr(0, el.size() - 1)),
                           el[el.size() - 1]
                                    )
                   );
               }
               return result;
           });


        this->registerConfigEntry<vector<int>, string>("numbersParameter", "100,8,8,100", Configurable::toVector<int>(','));
        this->registerConfigEntry<vector<vector<int>>, string>("vectorVectorExample", "1,2;4,8", Configurable::toVectorVector<int>(';', ','));
    };
};

TEST_CASE("ConfigurableTests") {
    // Capture cout
    streambuf *defaultOutBuffer = cout.rdbuf();
    ostringstream capturedOutBuffer;
    cout.rdbuf(capturedOutBuffer.rdbuf());
    capturedOutBuffer.str("");

    SECTION("Should parse the config file values into the configMap") {
        ConcreteConfigurable configurable;
        configurable.setConfigFromFile("../tests/mockFiles/testConfig.json");

        REQUIRE(configurable.getConfigEntry<int>("integerParameter") == 42);
        REQUIRE(configurable.getConfigEntry<string>("stringParameter") == "configValue");
        REQUIRE(configurable.getConfigEntry<bool>("boolParameter") == true);
    }

    SECTION("Should use the default value, if the parameter is not specified in the config file") {
        ConcreteConfigurable configurable;
        configurable.setConfigFromFile("../tests/mockFiles/partiallyCompleteTestConfig.json");

        REQUIRE(configurable.getConfigEntry<int>("integerParameter") == 42);
        REQUIRE(configurable.getConfigEntry<string>("stringParameter") == "defaultValue");
        REQUIRE(configurable.getConfigEntry<bool>("boolParameter") == false);
    }

    SECTION("Should print a notice, when using a default parameter") {
        ConcreteConfigurable configurable;
        configurable.setConfigFromFile("../tests/mockFiles/partiallyCompleteTestConfig.json");

        REQUIRE(capturedOutBuffer.str() == "Use default value for config parameter: stringParameter\n"
                                           "Use default value for config parameter: boolParameter\n"
                                           "Use default value for config parameter: complexType\n");
    }

    SECTION("Should determine, if a config option exists") {
        ConcreteConfigurable configurable;
        configurable.setConfigFromFile("../tests/mockFiles/testConfig.json");

        REQUIRE(configurable.containsConfigEntry("integerParameter") == true);
        REQUIRE(configurable.containsConfigEntry("unknownParameter") == false);
    }

    SECTION("Should convert config parameters into their correct type and form using the converter function") {
        ConcreteConfigurable configurable;
        configurable.setConfigFromFile("../tests/mockFiles/testConfig.json");

        REQUIRE(configurable.getConfigEntry<vector<int>>("complexType") == vector<int>{1, 1, 0, 1});
        REQUIRE(configurable.getConfigEntry<vector<pair<int, char>>>("readsParameter") == vector<pair<int, char>>{
                {100, 'R'},
                {8,   'B'},
                {8,   'B'},
                {100, 'R'},
                {10,  'R'}
        });
    }

    SECTION("Should use the provided ::toVector Function") {
        ConcreteConfigurable configurable;
        configurable.setConfigFromFile("../tests/mockFiles/testConfig.json");

        REQUIRE(configurable.getConfigEntry<vector<int>>("numbersParameter") == vector<int>{100, 8, 8, 100});
    }

    SECTION("Should extend the function 'setConfigFromFile' to include the fallback-ConfigMap provided in the parameter list") {
        std::map<std::string, boost::any> testConfigMap;

        //configure values for testConfigMap
        boost::any a = 100;
        boost::any b = 200;
        testConfigMap["testValue1"] = a;
        testConfigMap["testValue2"] = b;


        ConcreteConfigurable configurable;
        //in this function call, the testConfigMap is appended to configurable.configMap
        configurable.setConfigFromFile("../tests/mockFiles/testConfig.json", testConfigMap);

        //check whether ConfigMap of configurable contains the values of the 'testConfigMap'
        REQUIRE(configurable.containsConfigEntry("testValue1"));
        REQUIRE(configurable.getConfigEntry<int>("testValue1") == 100);
        REQUIRE(configurable.getConfigEntry<int>("testValue2") == 200);

        //check whether ConfigMap of configurable contains the values of the 'testConfigMap'
        REQUIRE(configurable.containsConfigEntry("integerParameter"));
        REQUIRE(configurable.getConfigEntry<int>("integerParameter") == 42);
        REQUIRE(configurable.getConfigEntry<bool>("boolParameter") == true);

    };

    // Reset cout
    cout.rdbuf(defaultOutBuffer);
}
