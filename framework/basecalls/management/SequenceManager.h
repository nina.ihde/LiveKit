#ifndef LIVEKIT_SEQUENCEREPRESENTATIONMANAGER_H
#define LIVEKIT_SEQUENCEREPRESENTATIONMANAGER_H

#include <stdint.h>
#include "../data_representation/SequenceContainer.h"
#include <map>

/**
 * The `SequenceManager` bundles all `Sequences` objects to carry a uniform representation and exposes an interface to get a specific `Sequences` object.
 */
class SequenceManager {
private:
    std::map<uint16_t, std::map<uint16_t, SequenceContainer>> sequenceRepresentations;
public:
    SequenceManager(std::vector<uint16_t> lanes, std::vector<uint16_t> tiles);

    SequenceContainer& getSequences(uint16_t lane, uint16_t tile);

    void writeAllSequencesToDisk(int cycle);
};

#endif //LIVEKIT_SEQUENCEREPRESENTATIONMANAGER_H
