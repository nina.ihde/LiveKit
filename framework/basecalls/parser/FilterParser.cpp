#include <boost/filesystem.hpp>
#include "FilterParser.h"
#include "../../utils.h"

using namespace std;

FilterParser::FilterParser(vector<uint16_t> &lanes, vector<uint16_t> &tiles, FilterRepresentation *filterRepresentation)
    : lanes(lanes), tiles(tiles), filterRepresentation(filterRepresentation) {}

void FilterParser::parse(uint16_t lane, uint16_t tile, vector<char> &rawFilterData) {
    // extract the number of reads
    uint32_t length;
    memcpy(&length, rawFilterData.data()+8, 4);
    // set the position pointer to the beginning of the data block
    int position = 12;

    vector<char> dataWithoutLength(rawFilterData.begin() + position, rawFilterData.end());
    this->filterRepresentation->addFilterData(lane, tile, dataWithoutLength);
}

void FilterParser::parse(uint16_t lane, uint16_t tile, const string &path, int numReads) {
    string filterFileName = getFilterFilename(lane, tile, path);
    if (boost::filesystem::exists(filterFileName)){
        std::vector<char> *rawFilterData;
        readBinaryFile(filterFileName, &rawFilterData);
        FilterParser::parse(lane, tile, *rawFilterData);
        delete rawFilterData;
    } else {
        this->filterRepresentation->initializeDefault(lane, tile, numReads);
    }

}
