#include "CompressedBclParser.h"
#include <cstring>
#include <zlib.h>
#include "../../utils.h"

using namespace std;

void CompressedBclParser::parse(uint16_t lane, uint16_t tile, uint16_t cycle, const string &bclPath) {
    string bclFileName = getBclFilename(lane, tile, cycle, bclPath, true);

    // decompress the bcl files
    gzFile file = gzopen(bclFileName.c_str(), "rb");
    if(file == nullptr){
        cerr << "Cannot read file: " + bclFileName << endl;
    }
    char unzipBuffer[8192];
    int unzippedBytes;
    vector<char> unzippedData;
    while (true) {
        unzippedBytes = gzread(file, unzipBuffer, 8192);
        if (unzippedBytes > 0) unzippedData.insert(unzippedData.end(), unzipBuffer, unzipBuffer + unzippedBytes);
        else break;
    }

    gzclose(file);

    // parse the unzipped bcl files
    BclParser::parse(lane, tile, unzippedData);
}
