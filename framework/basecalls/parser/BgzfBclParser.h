#ifndef LIVEKIT_BGZFBCLPARSER_H
#define LIVEKIT_BGZFBCLPARSER_H

#include "../data_representation/BclRepresentation.h"
#include "BclParser.h"

class BgzfBclParser : public BclParser {

public:
    // Filter files are currently not supported for the bgzf format

    BgzfBclParser(BclRepresentation *bclRepresentation) : BclParser(bclRepresentation) {}

    void parse(uint16_t lane, uint16_t tile, uint16_t cycle, const std::string &bclPath) override;

private:

    std::map<uint32_t, uint32_t> parseTileNumberToReadCount(const std::string &bciPath);

    std::vector<char> parseReadsByDecompressingFile(const std::string bclBgzfPath, const std::string bciPath, uint16_t tile);
};

#endif //LIVEKIT_BGZFBCLPARSER_H
