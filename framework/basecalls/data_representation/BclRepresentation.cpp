#include <vector>
#include <string>
#include "BclRepresentation.h"

using namespace std;

BclRepresentation::BclRepresentation(vector<uint16_t> &lanes, vector<uint16_t> &tiles, uint16_t bufferSize) : bufferSize(bufferSize) {
    // initialize the empty datastructure
    for (auto lane : lanes) {
        this->bclData[lane] =  map<uint16_t, deque<BCL>>();
        for (auto tile : tiles) {
            this->bclData[lane][tile] =  deque<BCL>();
        }
    }
};

deque<BCL> BclRepresentation::getMostRecentBcls(uint16_t lane) {
    // returns the most recent BCLs for each tile of a lane
    deque<BCL> mostRecent;
    for (auto &tileEntry : this->bclData[lane])
        if (!tileEntry.second.empty())
            mostRecent.push_back(tileEntry.second[0]);

    return mostRecent;
};

const BCL &BclRepresentation::getBcl(uint16_t lane, uint16_t tile, uint16_t cycleOffset) {
    if ((unsigned long)cycleOffset < this->bclData[lane][tile].size())
        return this->bclData[lane][tile][cycleOffset];
    string errorMessage =
            "The BCL representation does only store the bcls of the last " + to_string(this->bufferSize) + " cycles";
    __throw_out_of_range(errorMessage.c_str());
}

const deque<BCL> &BclRepresentation::getBcls(uint16_t lane, uint16_t tile) {
    return this->bclData[lane][tile];
}

BCLDATA &BclRepresentation::getBclData() {
    return this->bclData;
}

void BclRepresentation::addBcl(uint16_t lane, uint16_t tile, BCL newBcl) {
    this->bclData[lane][tile].push_front(newBcl);
    if (this->bclData[lane][tile].size() > bufferSize)
        this->bclData[lane][tile].pop_back(); // removes the oldest BCL in the buffer
}

void BclRepresentation::setBufferSize(uint16_t size) {
    this->bufferSize = size;
}

int BclRepresentation::getBufferSize() {
    return this->bufferSize;
}

uint32_t BclRepresentation::getBclSize(uint16_t lane, uint16_t tile) {
    return this->bclData[lane][tile][0].size();
}