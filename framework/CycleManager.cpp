#include "CycleManager.h"
#include <boost/algorithm/string/split.hpp>

using namespace std;

CycleManager::CycleManager(vector<pair<int,char>> reads) : reads(reads), cycleCount(0), currentCycle(1), currentReadCycle(1), currentReadId(0), currentMateId(1), mateCount(0), cyclesToSkip() {
    for (auto read : this->reads) {
        this->cycleCount += read.first;

        if (read.second == 'R') {
            this->cyclesToSkip.push(this->cycleCount);
            this->mateCount++;
        }
    }
}

int CycleManager::getCurrentCycle() {
    return this->currentCycle;
}

void CycleManager::incrementCurrentCycle() {
    this->currentCycle++;
    this->currentReadCycle++;

    if (this->getCurrentReadLength() < this->currentReadCycle) {
        this->currentReadCycle = 1;
        this->currentReadId++;

        if (this->reads[this->currentReadId].second == 'R') {
            this->currentMateId++;
        }
    }

    if (this->currentCycle == this->cyclesToSkip.front()) {
        this->cyclesToSkip.pop();
        // TODO: understand the consequences of the content of 'cyclesToSkip'!
        // this->incrementCurrentCycle();
    }
}

int CycleManager::getCycleCount() {
    return this->cycleCount;
}

int CycleManager::getCurrentReadCycle() {
    return this->currentReadCycle;
}

int CycleManager::getCurrentReadId() {
    return this->currentReadId;
}

int CycleManager::getCurrentReadLength() {
    return this->reads[this->currentReadId].first;
}

bool CycleManager::isBarcodeCycle() {
    return 'B' == this->reads[this->getCurrentReadId()].second;
}

int CycleManager::getCurrentMateId() {
    if (this->reads[this->currentReadId].second == 'B') {
        return 0;
    }

    return this->currentMateId;
}

int CycleManager::getMateCount() {
    return this->mateCount;
}
