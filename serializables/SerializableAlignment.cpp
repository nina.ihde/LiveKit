#include "SerializableAlignment.h"
#include "../plugins/HiLive_sharedLibraries/tools.h"
#include "../plugins/HiLive_sharedLibraries/tools_static.h"
#include "../plugins/HiLive_sharedLibraries/alnstream.h"

using namespace std;

void SerializableAlignment::serializeData(char *serializedSpace) {
    auto blockSize = this->settings->getConfigEntry<uint64_t>("blockSize");
    auto compressedFormat = this->settings->getConfigEntry<uint8_t>("compressedFormat");
    auto tempDir = this->settings->getConfigEntry<string>("tempDir");

    SERIALIZE_VALUE(this->lane);
    SERIALIZE_VALUE(this->tile);
    SERIALIZE_VALUE(this->rlen);
    SERIALIZE_VALUE(this->mate);
    SERIALIZE_VALUE(this->readId);
    SERIALIZE_VALUE(this->numReads);
    SERIALIZE_VALUE(this->currentReadCycle);
    SERIALIZE_VALUE(blockSize);
    SERIALIZE_VALUE(compressedFormat);

    unsigned long size = tempDir.size();
    memcpy(serializedSpace, &size, sizeof(size));
    serializedSpace += sizeof(size);
    memcpy(serializedSpace, tempDir.c_str(), size);

    string outputFilename = get_align_fname(this->lane, this->tile, this->currentReadCycle, this->mate, tempDir);
    outputFilename += ".fragment";
    this->writeAlignFile(outputFilename);
}

void  SerializableAlignment::writeAlignFile(string &outputFilename) {
    auto blockSize = this->settings->getConfigEntry<uint64_t>("blockSize");
    auto compressedFormat = this->settings->getConfigEntry<uint8_t>("compressedFormat");

    oAlnStream output(
        this->lane,
        this->tile,
        this->currentReadCycle,
        this->rlen,
        this->numReads,
        blockSize,
        compressedFormat
    );

    output.open(outputFilename);

    for (auto alignment : this->alignments) output.write_alignment(alignment);

    output.close();
}

void SerializableAlignment::deserializeData(char *serializedSpace) {
    uint64_t blockSize;
    uint8_t compressedFormat;
    string tempDir;

    DESERIALIZE_VALUE(this->lane);
    DESERIALIZE_VALUE(this->tile);
    DESERIALIZE_VALUE(this->rlen);
    DESERIALIZE_VALUE(this->mate);
    DESERIALIZE_VALUE(this->readId);
    DESERIALIZE_VALUE(this->numReads);
    DESERIALIZE_VALUE(this->currentReadCycle);
    DESERIALIZE_VALUE(blockSize);
    DESERIALIZE_VALUE(compressedFormat);

    unsigned long size;
    memcpy(&size, serializedSpace, sizeof(size));
    serializedSpace += sizeof(size);
    auto tempDirName = (char *) malloc(size);
    memcpy(tempDirName, serializedSpace, size);

    tempDir = string(tempDirName, tempDirName + size);

    std::string in_fname = get_align_fname(this->lane, this->tile, this->currentReadCycle, this->mate, tempDir);
    in_fname += ".fragment";
    iAlnStream input ( blockSize, compressedFormat, nullptr );
    input.open(in_fname);

    this->alignments.reserve(this->numReads);

    for (unsigned long i = 0; i < this->numReads; i++)
        this->alignments.push_back(input.get_alignment());

    input.close();

    remove(in_fname.c_str());

    this->alignmentPointersInvalid = true;
}

unsigned long SerializableAlignment::serializableDataSize() {
    string tempDir = this->settings->getConfigEntry<string>("tempDir");

    unsigned long serializableSize =
        sizeof(this->lane)
            + sizeof(this->tile)
            + sizeof(this->rlen)
            + sizeof(this->mate)
            + sizeof(this->numReads)
            + sizeof(this->currentReadCycle)
            + sizeof(uint64_t) // blockSize
            + sizeof(uint8_t)  // compressedFormat
            + sizeof(unsigned long)
            + tempDir.size();
    return serializableSize;
}

void SerializableAlignment::freeData() {
    for (auto alignment : this->alignments) delete alignment;

    this->alignments.clear();
}

void SerializableAlignment::create_directories() {
    std::ostringstream path_stream;
    path_stream << this->settings->getConfigEntry<string>("tempDir");

    path_stream << "/L00" << lane;

    boost::filesystem::create_directories(path_stream.str());
    boost::filesystem::create_directories(this->settings->getConfigEntry<string>("outDir"));
}

void SerializableAlignment::init_alignment(Configurable *newSettings) {
    // After serialization, the pointer to settings may be incorrect, so we have to set it again
    this->settings = newSettings;

    (this->memorySaveMode) ? this->init_alignment_on_disk() : this->init_alignment_in_memory();
}

void SerializableAlignment::init_alignment_in_memory() {
    string tempDir = this->settings->getConfigEntry<string>("tempDir");
    ReadAlignmentSettings readAlignmentSettings = ReadAlignment::generateSettingsfromConfigurable(settings);

    // write empty read alignments for each read
    for (uint32_t i = 0; i < this->numReads; ++i)
        alignments.push_back(new ReadAlignment(this->rlen, 0, &readAlignmentSettings));
}

void SerializableAlignment::init_alignment_on_disk() {
    string tempDir = this->settings->getConfigEntry<string>("tempDir");

    // open output alignment stream
    std::string out_fname = get_align_fname(lane, tile, 0, mate, tempDir);
    oAlnStream output (
        this->lane,
        this->tile,
        0,
        this->rlen,
        this->numReads,
        this->settings->getConfigEntry<uint64_t>("blockSize"),
        this->settings->getConfigEntry<uint8_t>("compressedFormat")
    );
    output.open(out_fname);

    ReadAlignmentSettings readAlignmentSettings = ReadAlignment::generateSettingsfromConfigurable(settings);

    // write empty read alignments for each read
    for (uint32_t i = 0; i < this->numReads; ++i) {
        ReadAlignment ra(this->rlen, 0, &readAlignmentSettings, nullptr);
        output.write_alignment(&ra);
    }

    if (!output.close()) std::cerr << "Error: Could not create initial alignment file." << std::endl;
}

uint64_t SerializableAlignment::extend_alignment(uint16_t cycle, bool keepAlnFile, SerializableIndex *index, BaseManager *newBaseManager, Configurable *newSettings, int &executedRealignments, int &seedsWithSoftclips) {
    this->currentReadCycle = cycle;

    // After de-/serialization, the pointers may be incorrect, so we have to set them again
    this->settings = newSettings;
    this->baseManager = newBaseManager;

    this->refreshAlignmentPointers();

    return (this->memorySaveMode)
           ? this->extend_alignment_on_disk(keepAlnFile, index, executedRealignments, seedsWithSoftclips)
           : this->extend_alignment_in_memory(keepAlnFile, index, executedRealignments, seedsWithSoftclips);
}

uint64_t SerializableAlignment::extend_alignment_in_memory(bool keepAlnFile, SerializableIndex *index, int &executedRealignments, int &seedsWithSoftclips) {
    ReadAlignmentSettings readAlignmentSettings = ReadAlignment::generateSettingsfromConfigurable(this->settings);

    const BCL &bcl = this->baseManager->getBcls(this->lane, this->tile).front();

    // Extend alignments 1 by 1
    auto numThreads = settings->getConfigEntry<uint16_t>("threadsPerTile");

    vector<thread> tile_worker;
    tile_worker.reserve(numThreads);

    vector<int> num_seeds(numThreads, 0);

    vector<char> &filterData = this->baseManager->getFilterData(this->lane, this->tile);

    // Distribute Alignment extension on multiple Threads
    for (int threadId = 0; threadId < numThreads; threadId++) {
        uint64_t startPoint = threadId * bcl.size() / numThreads;
        uint64_t endPoint   = min( ( threadId + 1 ) * bcl.size() / numThreads, bcl.size());
        tile_worker.emplace_back([&](uint64_t startPoint, uint64_t endPoint, int threadId){
          for (uint64_t i = startPoint; i < endPoint; i ++) {
              alignments[i]->setSettings(&readAlignmentSettings);
              if (filterData[i] == 0) {
                  alignments[i]->disable();
              } else {
                  alignments[i]->cycle = this->currentReadCycle;
                  alignments[i]->extend_alignment(bcl[i], index, executedRealignments, seedsWithSoftclips);
                  num_seeds[threadId] += alignments[i]->seeds.size();
              }
          }
        }, startPoint, endPoint, threadId);
    }

    for (auto &worker : tile_worker) worker.join();

    uint64_t sum_seeds = 0;
    for (auto& n : num_seeds) sum_seeds += n;


    if ( keepAlnFile ) {
        string tempDir = this->settings->getConfigEntry<string>("tempDir");
        string outputFilename = get_align_fname(this->lane, this->tile, this->currentReadCycle, this->mate, tempDir);
        this->writeAlignFile(outputFilename);
    }

    return sum_seeds;
}

uint64_t SerializableAlignment::extend_alignment_on_disk(bool keepAlnFile, SerializableIndex *index, int &executedRealignments, int &seedsWithSoftclips) {
    auto tempDir  = this->settings->getConfigEntry<string>("tempDir");
    auto blockSize   = this->settings->getConfigEntry<uint64_t>("blockSize");
    auto format = this->settings->getConfigEntry<uint8_t>("compressedFormat");

    // 1. Open the input file
    //-----------------------

    ReadAlignmentSettings readAlignmentSettings = ReadAlignment::generateSettingsfromConfigurable(this->settings);

    string in_fname = get_align_fname(this->lane, this->tile, this->currentReadCycle-1, mate, tempDir);
    iAlnStream input(blockSize, format, &readAlignmentSettings);
    input.open(in_fname);

    const BCL &bcl = this->baseManager->getBcls(lane, tile).front();
    vector<char> &filterData = this->baseManager->getFilterData(this->lane, this->tile);
    uint64_t num_reads = bcl.size(); // = input.get_num_reads();

    // 2. Open output stream
    //----------------------------------------------------------

    string out_fname = get_align_fname(this->lane, this->tile, this->currentReadCycle, mate, tempDir);
    oAlnStream output (this->lane, this->tile, this->currentReadCycle, this->rlen, num_reads, blockSize, format);
    output.open(out_fname);

    // 3. Extend alignments 1 by 1
    //-------------------------------------------------

    uint64_t num_seeds = 0;
    for (uint64_t i = 0; i < bcl.size(); i++) {
        ReadAlignment* ra = input.get_alignment();
        if (filterData[i] == 0) ra->disable();
        else {
            ra->setRead(this->baseManager->getSequence(lane, tile, i).getAddressAt(this->readId));
            ra->extend_alignment(bcl[i], index, executedRealignments, seedsWithSoftclips);
            num_seeds += ra->seeds.size();
        }
        output.write_alignment(ra);
        delete ra;
    }

    // 4. Close files
    //-------------------------------------------------
    if (!(input.close() && output.close())) cerr << "Could not finish alignment!" << endl;

    // 5. Delete old alignment file, if requested
    //-------------------------------------------

    if ( !keepAlnFile ) remove(in_fname.c_str());

    return num_seeds;
}


void SerializableAlignment::extend_barcode(uint16_t bc_cycle, unsigned long currentReadLength, bool keepAlnFile, BaseManager *newBaseManager, Configurable *newSettings) {
    // After serialization, the pointer to settings may be incorrect, so we have to set it again
    this->settings = newSettings;
    this->baseManager = newBaseManager;

    this->refreshAlignmentPointers();

    (this->memorySaveMode)
    ? this->extend_barcode_on_disk  (bc_cycle, currentReadLength)
    : this->extend_barcode_in_memory(bc_cycle, currentReadLength, keepAlnFile);
}

void SerializableAlignment::extend_barcode_in_memory(uint16_t bc_cycle, unsigned long currentReadLength, bool keepAlnFile) {
    ReadAlignmentSettings readAlignmentSettings = ReadAlignment::generateSettingsfromConfigurable(this->settings);

    BCL bcl = this->baseManager->getBcls(lane, tile).front();
    bool keepAllBarcodes = this->settings->getConfigEntry<bool>("keepAllBarcodes");

    // Extend barcode sequence
    for (uint64_t i = 0; i < bcl.size(); i++) {
        alignments[i]->setSettings(&readAlignmentSettings);
        alignments[i]->appendNucleotideToSequenceStoreVector(bcl[i], true);

        // filter invalid barcodes if new barcode fragment is completed
        // TODO: Is done for each mate. Check if it's worth to change it (runtime should not be too high?)
        if ( !keepAllBarcodes
            && bc_cycle == currentReadLength
            && alignments[i]->getBarcodeIndex() == UNDETERMINED
            ) alignments[i]->disable();
    }

    if ( keepAlnFile ) {
        string tempDir = this->settings->getConfigEntry<string>("tempDir");
        string outputFilename = get_align_fname(this->lane, this->tile, this->currentReadCycle, this->mate, tempDir);
        this->writeAlignFile(outputFilename);
    }
}

void SerializableAlignment::extend_barcode_on_disk(uint16_t bc_cycle, unsigned long currentReadLength) {
    auto tempDir  = this->settings->getConfigEntry<string>("tempDir");
    auto blockSize   = this->settings->getConfigEntry<uint64_t>("blockSize");
    auto format = this->settings->getConfigEntry<uint8_t>("compressedFormat");

    ReadAlignmentSettings readAlignmentSettings = ReadAlignment::generateSettingsfromConfigurable(this->settings);

    // 1. Open the input file
    //-----------------------

    std::string in_fname = get_align_fname(this->lane, this->tile, this->currentReadCycle, mate, tempDir);
    iAlnStream input ( blockSize, format, &readAlignmentSettings );
    input.open(in_fname);

    BCL bcl = this->baseManager->getBcls(lane, tile).front();
    uint32_t num_reads = input.get_num_reads();
    bool keepAllBarcodes = this->settings->getConfigEntry<bool>("keepAllBarcodes");

    // 2. Open output stream
    //----------------------------------------------------------
    std::string out_fname = in_fname + ".temp";
    oAlnStream output (this->lane, this->tile, this->currentReadCycle, input.get_rlen(), num_reads, blockSize, format);
    output.open(out_fname);

    // 3. Extend barcode sequence
    //-------------------------------------------------

    for (auto &base : bcl) {
        ReadAlignment* ra = input.get_alignment();
        ra->appendNucleotideToSequenceStoreVector(base, true);

        // filter invalid barcodes if new barcode fragment is completed
        // TODO: Is done for each mate. Check if it's worth to change it (runtime should not be too high?)
        if ( !keepAllBarcodes && bc_cycle == currentReadLength && ra->getBarcodeIndex() == UNDETERMINED )
            ra->disable();

        output.write_alignment(ra);
        delete ra;
    }

    // 4. Close files
    //-------------------------------------------------
    if (!(input.close() && output.close())) std::cerr << "Could not finish alignment!" << std::endl;

    // 5. Move temp out file to the original file.
    //-------------------------------------------
    atomic_rename(out_fname.c_str(), in_fname.c_str());
}

void SerializableAlignment::refreshAlignmentPointers() {
    if (!this->alignmentPointersInvalid) return;

    uint64_t i = 0;
    SequenceContainer &sequences = this->baseManager->getSequences(lane, tile);
    for (auto &alignment : alignments)
        alignment->setRead(sequences.getSequence(i++).getAddressAt(this->readId));

    this->alignmentPointersInvalid = false;
}
