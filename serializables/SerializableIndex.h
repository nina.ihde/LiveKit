#ifndef LIVEKIT_SERIALIZABLEINDEX_H
#define LIVEKIT_SERIALIZABLEINDEX_H

#include <seqan/sequence.h>
#include <seqan/index.h>
#include <boost/filesystem.hpp>

#include "../plugins/HiLive_sharedLibraries/tools_static.h"
#include "../framework/Serializable.h"

/**
 * FM-Index Config.
 */
typedef seqan::FastFMIndexConfig<void, uint64_t, 2, 1> FMIConfig;

/**
 * Bidirectional FM-Index data type.
 */
typedef seqan::Index<seqan::StringSet<seqan::DnaString>, seqan::BidirectionalIndex<seqan::FMIndex<void, FMIConfig> >> BidirectionalFMIndex;

class SerializableIndex : public Serializable {
private:
    /** Names of the sequences in the index. */
    std::vector<std::string> seq_names;

    /** Lengths of the sequences in the index. */
    std::vector<uint32_t> seq_lengths;

protected:
    const std::string name() const override { return "index"; };

public:
    /** The FM index itself. */
    BidirectionalFMIndex *index;

    std::string indexName;
    seqan::StringSet<seqan::DnaString> *allSequences;
    bool sequencesRemoved = false;

    SerializableIndex(unsigned long size, char *serializedSpace, bool deleteFiles = true);

    /**
	 * Function to build the fm-index from a input (multi-)fasta file.
	 * @param fastaName Name of the input fasta file
	 * @param indexName Name of the output index file(s)
	 * @param convertSpaces If true, inner spaces are converted to underscores
	 * @param trimIds If true, the id is trimmed after the first whitespace
	 * @author Tobias Loka
	 */
    SerializableIndex(std::string fastaName, std::string indexName, bool convertSpaces, bool trimIds);

    /**
     * Read a fasta file and load the sequences to the HiLive data structure.
     * @param fname Name of the input fasta file
     * @param convert_spaces If true, inner spaces are converted to underscores
     * @param trim_ids If true, the id is trimmed after the first whitespace
     * @return Number of loaded sequences
     * @author Martin Lindner, Tobias Loka
     */
    int
    add_fasta(const std::string &fname, bool convert_spaces, bool trim_ids, seqan::StringSet<seqan::DnaString> &seqs);

    /**
     * Save the Sequence names to file.
     * @param iname The name of the output index file(s)
     * @return 0 if successful, error code otherwise
     * @author Tobias Loka
     */
    int save_seqnames(const std::string &iname);

    /**
	 * Save the Sequence lengths to file.
	 * @param iname The name of the output index file(s)
	 * @return 0 if successful, error code otherwise
	 * @author Tobias Loka
	 */
    int save_seqlengths(const std::string &iname);

    /**
	 * Load the sequence names of the fm index
	 * @param iname Name of the index file(s)
	 * @return 0 on success, other value on failure
	 */
    int load_seqnames(const std::string &iname);

    /**
	 * Load the sequence lengths of the fm index
	 * @param iname Name of the index file(s)
	 * @return 0 on success, other value on failure
	 */
    int load_seqlengths(const std::string &iname);

    /**
	 * Get the sequence name by using the fm index sequence id.
	 * !! The gid does NOT equal the index of the seq_names array !!
	 * @param gid Sequence id as given in the fm index
	 * @return Name of the sequence
	 */
    std::string getSequenceName(uint32_t gid) { return this->seq_names[gid / 2]; };

    /**
     * Get the sequence length by using the fm index sequence id.
     * !! The gid does NOT equal the index of the seq_names array !!
     * @param gid Sequence id as given in the fm index
     * @return Length of the sequence
     */
    uint32_t getSequenceLength(uint16_t gid) { return this->seq_lengths[gid / 2]; };

    /**
     * Get the sequence names list
     * @return The complete list of sequence names
     */
    std::vector<std::string> &getSeqNames() { return this->seq_names; };

    /**
     * Get the sequence lengths vector
     * @return The complete vector of sequence lengths
     */
    std::vector<uint32_t> &getSeqLengths() { return this->seq_lengths; };

    /**
     * Get the number of sequences
     * @return The number of sequences in the index (real number of sequences, not the number of sequences in the fm index)
     */
    uint32_t getNumSequences() { return this->seq_lengths.size(); };

    /**
     * Check if an alignment is on the reverse strand.
     * @param gid Sequence id as given in the fm index
     * @return true, if alignment is on the reverse strand
     */
    bool isReverse(uint32_t gid) { return gid % 2 == 1; };

    unsigned long serializableDataSize() override;

    void serializeData(char *serializedSpace) override;

    void deserializeData(char *serializedSpace) override;

    void freeData() override;

    void deserializeWithoutDeletion(char *serializedSpace);

    ~SerializableIndex() override;
};

#endif //LIVEKIT_SERIALIZABLEINDEX_H
