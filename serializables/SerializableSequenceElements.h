#include "../plugins/HiLive_sharedLibraries/headers.h"
#include "../plugins/HiLive_sharedLibraries/definitions.h"
#include "SerializableVector.h"

#ifndef LIVEKIT_SEQUENCEELEMENTSWRAPPER_H
#define LIVEKIT_SEQUENCEELEMENTSWRAPPER_H


class SerializableSequenceElements : public Serializable {
private:
    SerializableVector<SequenceElement> seqs;
    uint16_t mateCount;

    const std::string name() const override { return "seqEl"; };


public:
    explicit SerializableSequenceElements(std::vector<SequenceElement> seqs);

    explicit SerializableSequenceElements(char *serializedSpace);

    SerializableVector<SequenceElement> getSeqs() const;

    SequenceElement getSeqById(uint16_t id) const;

    SequenceElement getSeqByMate(uint16_t mate) const;

    void setMateCount(uint16_t mateCount);

    /** Get the current sequencing cycle using the current alignment cycle and read number.
     * @param cycle The read cycle.
     * @param seq_id The sequence id (:= id of the respective element in SequenceElementWrapper)
     * @return The sequencing cycle.
     * @author Tobias Loka
     */
    uint16_t getSeqCycle(uint16_t cycle, uint16_t seq_id);

    /**
     * Get the cycle of a mate for a given sequencing cycle.
     * When the mate is completely finished in the given cycle, return its total sequence length.
     * @param mate_number Mate of interest.
     * @param seq_cycle The sequencing cycle.
     * @return Cycle of the mate in the given sequencing cycle.
     * @author Tobias Loka
     */
    uint16_t getMateCycle(uint16_t mate_number, uint16_t seq_cycle);

    unsigned long serializableDataSize() override;

    void serializeData(char *serializedSpace) override;

    void deserializeData(char *serializedSpace) override;

    void freeData() override ;
};


#endif //LIVEKIT_SEQUENCEELEMENTSWRAPPER_H
