#include <utility>
#include "SerializableIndex.h"

using namespace std;

SerializableIndex::SerializableIndex(string fastaName, string indexName, bool convertSpaces, bool trimIds) {
    /** Sequences in the index. */
    seqan::StringSet<seqan::DnaString> seqs;

    try {
        if (this->add_fasta(fastaName, convertSpaces, trimIds, seqs) == 0) {
            cerr << "Reading input file failed (no sequences found)." << endl;
        }
    } catch (ifstream::failure &readErr) {
        cerr << readErr.what() << endl;
        cerr << "Reading input file failed." << endl;
    }

    // modify sequences for index building (reverse and add reverse complement)
    //seqan::StringSet<seqan::DnaString> allSequences; /* Sequences in the index. */
    allSequences = new seqan::StringSet<seqan::DnaString>;

    for (uint16_t i = 0; i < length(seqs); i++) {
        seqan::ModifiedString<seqan::DnaString, seqan::ModReverse> rev(seqs[i]);
        seqan::appendValue(*allSequences, rev);
        seqan::complement(seqs[i]);
        seqan::appendValue(*allSequences, seqs[i]);
    }

    this->index = new BidirectionalFMIndex(*allSequences);
    this->indexName = move(indexName);

    if (!seqan::indexCreate(*this->index)) {
        cerr << "Index building failed." << endl;
    }
}

SerializableIndex::SerializableIndex(unsigned long size, char *serializedSpace, bool deleteFiles) {
    this->sequencesRemoved = true;
    if (deleteFiles)
        this->deserialize(serializedSpace);
    else {
        this->deserializeName(&serializedSpace);
        this->deserializeWithoutDeletion(serializedSpace);
    }
}

unsigned long SerializableIndex::serializableDataSize() {
    return sizeof(unsigned long) + indexName.size();
}

void SerializableIndex::freeData() {
    // TODO: Make this pretty!
    if (!this->sequencesRemoved) {
        this->sequencesRemoved = true;
        delete this->allSequences;
    }

    delete this->index;
    this->index = nullptr;
}

void SerializableIndex::serializeData(char *serializedSpace) {
    unsigned long size = this->indexName.size();
    memcpy(serializedSpace, &size, sizeof(unsigned long));
    serializedSpace += sizeof(unsigned long);

    memcpy(serializedSpace, this->indexName.c_str(), size);

    const char *filename = this->indexName.c_str();

    const char *path = this->indexName.substr(0, this->indexName.find_last_of("\\/")).c_str();
    boost::filesystem::create_directories(path);

    if (!(this->save_seqnames(this->indexName) && this->save_seqlengths(this->indexName))) {
        cerr << "Writing metadata failed." << endl;
    }

    if (seqan::save(*this->index, filename) != 1) {
        cerr << "Writing index to file " << this->indexName << " failed." << endl;
    }
}

void SerializableIndex::deserializeWithoutDeletion(char *serializedSpace) {
    unsigned long size;
    memcpy(&size, serializedSpace, sizeof(unsigned long));
    serializedSpace += sizeof(unsigned long);
    auto tmpIndexName = (char *) malloc(size);
    memcpy(tmpIndexName, serializedSpace, size);

    this->indexName = string(tmpIndexName, tmpIndexName + size);

    const char *filename = this->indexName.c_str();

    this->load_seqnames(this->indexName);
    this->load_seqlengths(this->indexName);

    this->index = new BidirectionalFMIndex();
    seqan::open(*this->index, filename);
}

void SerializableIndex::deserializeData(char *serializedSpace) {
    this->deserializeWithoutDeletion(serializedSpace);

    const char *path = this->indexName.substr(0, this->indexName.find_last_of("\\/")).c_str();
    boost::filesystem::remove_all(path);
}

int SerializableIndex::save_seqnames(const string &iname) {
    string file_name = iname;
    file_name += ".seqnames";
    // get total amount of bytes needed
    uint32_t data_size = sizeof(uint32_t); // number of sequences
    for (uint32_t i = 0; i < this->seq_names.size(); i++) {
        data_size += sizeof(uint32_t); // length of the name
        data_size += this->seq_names[i].size();
    }
    // init data vector
    vector<char> data(data_size);
    char *d = data.data();

    // write number of sequences
    uint32_t size = this->seq_names.size();
    memcpy(d, &size, sizeof(uint32_t));
    d += sizeof(uint32_t);

    // write all sequence names
    for (uint32_t i = 0; i < this->seq_names.size(); i++) {
        // write sequence name length
        size = this->seq_names[i].size();
        memcpy(d, &size, sizeof(uint32_t));
        d += sizeof(uint32_t);
        const char *seqn = this->seq_names[i].c_str();
        // write sequence name
        memcpy(d, seqn, this->seq_names[i].size());
        d += this->seq_names[i].size();
    }

    // write data to file
    uint32_t written = write_binary_file(file_name, data);

    return (written == data_size);
}

int SerializableIndex::save_seqlengths(const string &iname) {

    string file_name = iname;
    file_name += ".seqlengths";

    // get total amount of bytes needed
    uint32_t data_size = sizeof(uint32_t); // number of sequences
    for (uint32_t i = 0; i < this->seq_lengths.size(); i++) {
        data_size += sizeof(uint32_t); // sequence length
    }

    // init data vector
    vector<char> data(data_size);
    char *d = data.data();

    // write number of sequences
    uint32_t seqlen = this->seq_lengths.size();
    memcpy(d, &seqlen, sizeof(uint32_t));
    d += sizeof(uint32_t);

    // write all sequence names
    for (uint32_t i = 0; i < this->seq_lengths.size(); i++) {

        // write sequence length
        seqlen = this->seq_lengths[i];
        memcpy(d, &seqlen, sizeof(uint32_t));
        d += sizeof(uint32_t);

    }

    // write data to file
    uint32_t written = write_binary_file(file_name, data);

    return (written == data_size);
}

int SerializableIndex::load_seqnames(const string &iname) {

    string file_name = iname;
    file_name += ".seqnames";

    vector<char> data = read_binary_file(file_name);
    char *d = data.data();

    uint32_t num_seqs;
    memcpy(&num_seqs, d, sizeof(uint32_t));
    d += sizeof(uint32_t);

    uint32_t curr_seqLength;
    for (uint32_t i = 0; i < num_seqs; i++) {

        memcpy(&curr_seqLength, d, sizeof(uint32_t));
        d += sizeof(uint32_t);

        char *seq_name = new char[curr_seqLength];

        memcpy(seq_name, d, curr_seqLength);
        d += curr_seqLength;

        // Don't know why the substring is necessary here, but without it there are strange artifacts in some names.
        this->seq_names.push_back(string(seq_name).substr(0, curr_seqLength));

    }
    return true;
}

int SerializableIndex::load_seqlengths(const string &iname) {
    string file_name = iname;
    file_name += ".seqlengths";

    vector<char> data = read_binary_file(file_name);
    char *d = data.data();

    uint32_t num_seqs;
    memcpy(&num_seqs, d, sizeof(uint32_t));
    d += sizeof(uint32_t);

    uint32_t seqlength;
    for (uint32_t i = 0; i < num_seqs; i++) {
        memcpy(&seqlength, d, sizeof(uint32_t));
        d += sizeof(uint32_t);

        this->seq_lengths.push_back(seqlength);
    }

    return true;
}

int SerializableIndex::add_fasta(const string &fname, bool convert_spaces, bool trim_ids,
                                 seqan::StringSet<seqan::DnaString> &seqs) {

#ifndef TEST_DEFINITIONS
    ios::sync_with_stdio(false);
    ifstream::sync_with_stdio(false);
#endif

    // open input fasta file
    ifstream infile(fname.c_str());
    assert(infile.is_open());

    string line;    // current line of fasta file
    string seq_name;    // name of the current sequence
    bool startNewSequence = false;    // true if last line was a header / new sequence begins
    seqan::DnaString newSeq = "";    // current DNA sequence

    while (getline(infile, line)) {

        // ignore empty lines
        if (line.length() == 0) { continue; };


        // check for Windows newline characters (just in case the fasta comes from Windows --> thanks Simon)
        if (line[line.length() - 1] == '\r') {
            line.erase(line.length() - 1);
        }

        // handle header line
        if (line[0] == '>') {

            // add last sequence to sequence vector (if exist)
            if (newSeq != "") {
                seqan::appendValue(seqs, newSeq);
            }

            // trim the header (remove leading and trailing whitespaces)
            trim(line);

            // handle sequence name options
            if (convert_spaces)
                replace(line.begin(), line.end(), ' ', '_');

            if (trim_ids)
                seq_name = line.substr(1, line.find(' ') - 1);
            else
                seq_name = line.substr(1, line.length() - 1);

            // add the sequence name to the sequence name vector
            this->seq_names.push_back(seq_name);

            // init new sequence fields
            this->seq_lengths.push_back(0);
            startNewSequence = true;
            newSeq = "";
        }

            // handle sequence lines
        else {
            // TODO: remove leading and trailling Ns

            // start new sequence
            if (startNewSequence) {
                newSeq = line;
                startNewSequence = false;
            }

                // extend existing sequence
            else {
                newSeq += line;
            }
            *(--this->seq_lengths.end()) += line.length();
        }
    }

    infile.close();

    // append the last sequence to the StringSet
    if (newSeq != "") {
        seqan::appendValue(seqs, newSeq);
    }

    return seq_lengths.size();
}

SerializableIndex::~SerializableIndex() {
    delete this->index;
}
